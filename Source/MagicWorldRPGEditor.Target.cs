// Copyright Alex Shabynin

using UnrealBuildTool;
using System.Collections.Generic;

public class MagicWorldRPGEditorTarget : TargetRules
{
	public MagicWorldRPGEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "MagicWorldRPG" } );
	}
}
