﻿#pragma once

#include "DrawDebugHelpers.h"

namespace Debug
{
static void Print(const FString& Msg, float DisplayTime, const FColor& Color = FColor::MakeRandomColor(), int32 InKey = -1)
{
	if(GEngine)
	{
		GEngine->AddOnScreenDebugMessage(InKey,DisplayTime,Color,Msg);
	}
	UE_LOG(LogTemp,Warning,TEXT("%s"),*Msg);
}
	
};
