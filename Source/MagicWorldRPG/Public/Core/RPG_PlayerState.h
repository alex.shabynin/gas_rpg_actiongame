// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "AbilitySystemInterface.h"
#include "RPG_PlayerState.generated.h"

class URPGLevelUpInfo;
DECLARE_MULTICAST_DELEGATE_OneParam(FOnPlayerStatChanged, int32 /*StatValue*/)
DECLARE_MULTICAST_DELEGATE_OneParam(FOnLevelChanged, int32 /*StatValue*/)


class ARPG_PlayerCharacter;
class UAttributeSet;
class UAbilitySystemComponent;

UCLASS()
class MAGICWORLDRPG_API ARPG_PlayerState : public APlayerState, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	ARPG_PlayerState();
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	UAttributeSet* GetAttributeSet() const {return AttributeSet;}
	FORCEINLINE int32 GetPlayerLevel() const {return Level;}
	FORCEINLINE int32 GetXP() const {return XP;}
	UFUNCTION(BlueprintPure,BlueprintCallable)
	FORCEINLINE int32 GetAttributePoints() const {return AttributePoints;}
	FORCEINLINE int32 GetSpellPoints() const {return SpellPoints;}

	FOnPlayerStatChanged OnXPChangedDelegate;
	FOnLevelChanged OnLevelChangedDelegate;
	FOnPlayerStatChanged OnAttributePointsChangedDelegate;
	FOnPlayerStatChanged OnSpellPointsChangedDelegate;
	
	void SetXP(int32 InXP);
	void AddToXP(int32 InXP);
	
	void SetLevel(int32 InLevel);
	void AddToLevel(int32 InLevel);

	void SetAttributePoints(int32 InAttributePoints);
	void AddAttributePoints(int32 InAttributePoints);

	void SetSpellPoints(int32 InSpellPoints);
	void AddSpellPoints(int32 InSpellPoints);

	UPROPERTY(EditDefaultsOnly,Category="AbilitySystem")
	TObjectPtr<URPGLevelUpInfo> LevelUpInfo;

protected:
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="AbilitySystem")
	TObjectPtr<UAbilitySystemComponent> AbilitySystemComponent;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="AbilitySystem")
	TObjectPtr<UAttributeSet> AttributeSet;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,ReplicatedUsing=OnRep_Level,Category="AbilitySystem")
	int32 Level = 1;
	UPROPERTY(VisibleAnywhere,ReplicatedUsing=OnRep_XP)
	int32 XP =0;
	UPROPERTY(VisibleAnywhere,ReplicatedUsing=OnRep_AttributePoints)
	int32 AttributePoints = 0;

	UPROPERTY(VisibleAnywhere,ReplicatedUsing=OnRep_SpellPoints)
	int32 SpellPoints = 0;
	
	UFUNCTION()
	void OnRep_Level(int32 OldLevel);

	UFUNCTION()
	void OnRep_XP(int32 OldXP);

	UFUNCTION()
	void OnRep_AttributePoints(int32 OldAttributePoints);

	UFUNCTION()
	void OnRep_SpellPoints(int32 OldSpellPoints);
};
