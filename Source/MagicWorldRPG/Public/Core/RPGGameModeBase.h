// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RPGGameModeBase.generated.h"

class URPGAbilityInfo;
class URPGCharacterClassInfo;
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API ARPGGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly,Category="Character Class Defaults")
	TObjectPtr<URPGCharacterClassInfo> CharacterClassInfo;

	UPROPERTY(EditDefaultsOnly,Category="Ability Information")
	TObjectPtr<URPGAbilityInfo> AbilityInfo;
	
};
