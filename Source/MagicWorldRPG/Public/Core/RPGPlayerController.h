// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RPGPlayerController.generated.h"

class ARPGMagicCircle;
class UDamageTextWidgetComp;
class USplineComponent;
class URPGAbilitySystemComponent;
struct FGameplayTag;
class URPG_InputConfig;
struct FInputActionValue;
/**
 * 
 */
class UInputMappingContext;
class UInputAction;
class IRPG_TargetInterface;
class UNiagaraSystem;

UCLASS()
class MAGICWORLDRPG_API ARPGPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ARPGPlayerController();

	virtual void PlayerTick(float DeltaTime) override;

	UFUNCTION(Client,Reliable)
	void ShowDamageAmount(float DamageAmount, ACharacter* TargetCharacter, bool bIsBlocked, bool bIsCritical);
	
	UFUNCTION(BlueprintCallable)
    void ShowMagicCircle(UMaterialInterface* DecalMaterial = nullptr);
    UFUNCTION(BlueprintCallable)
    void HideMagicCircle();
protected:

	virtual void BeginPlay() override;
	
	void AddInputMappingContext(UInputMappingContext* ContextToAdd, int32 InPriority);
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Input")
	TObjectPtr<UInputMappingContext> PlayerMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Input")
	TObjectPtr<UInputAction> MoveAction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Input")
	TObjectPtr<UInputAction> ShiftAction;
	
	virtual void SetupInputComponent() override;

	void Move(const FInputActionValue& Value);
	void ShiftPressed();
	void ShiftReleased();
	bool bShiftKeyDown = false;
	
	UEnhancedInputComponent* EnhancedInputComponent;

	
	
private:

	UPROPERTY(EditDefaultsOnly,Category="Niagara FX")
	TObjectPtr<UNiagaraSystem> ClickNiagaraSystem;
	
	void CursorTrace();

	IRPG_TargetInterface* LastActor;
	IRPG_TargetInterface* ThisActor;

	FHitResult CursorHit;

	void AbilityInputTagPressed(FGameplayTag InputTag);
	void AbilityInputTagReleased(FGameplayTag InputTag);
	void AbilityInputTagHold(FGameplayTag InputTag);

	UPROPERTY(EditDefaultsOnly,Category="Input")
	TObjectPtr<URPG_InputConfig> InputConfig;

	UPROPERTY()
	TObjectPtr<URPGAbilitySystemComponent> RPGAbilitySystemComponent;

	URPGAbilitySystemComponent* GetASC();

#pragma region ClickToMove Properties
	
	FVector CachedDestination = FVector::ZeroVector;
	float FollowTime = 0.f;
	float ShortPressThreshold = 0.5f;
	bool bAutoRunning = false;
	bool bTargeting = false;

	UPROPERTY(EditDefaultsOnly)
	float AutoRunAcceptanceRadius = 50.f;

	UPROPERTY(VisibleAnywhere)
	TObjectPtr<USplineComponent> Spline;

	void AutoRun();

#pragma endregion

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UDamageTextWidgetComp> DamageTextComp;

	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ARPGMagicCircle> MagicCircleClass;

	UPROPERTY()
	TObjectPtr<ARPGMagicCircle> MagicCircle;

	void UpdateMagicCircleLocation();
};

