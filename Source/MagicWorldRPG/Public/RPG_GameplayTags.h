// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"

/**
 * Gameplay Tags
 *
 * Singleton containing native Gameplay Tags
 */

struct FRPG_GameplayTags
{

public:
	
    static const FRPG_GameplayTags& Get() {return GameplayTags;}
    static void InitializeNativeGameplayTags();

#pragma region Primary Attributes
	
	FGameplayTag Attributes_Primary_Strength;
	FGameplayTag Attributes_Primary_Intelligence;
	FGameplayTag Attributes_Primary_Resilience;
	FGameplayTag Attributes_Primary_Vigor;
	
#pragma endregion

#pragma region Secondary Attributes
	
	FGameplayTag Attributes_Secondary_Armor;
	FGameplayTag Attributes_Secondary_ArmorPenetration;
	FGameplayTag Attributes_Secondary_BlockChance;
	FGameplayTag Attributes_Secondary_CritHitChance;
	FGameplayTag Attributes_Secondary_CritHitDamage;
	FGameplayTag Attributes_Secondary_CritHitResistance;
	FGameplayTag Attributes_Secondary_HealthRegen;
	FGameplayTag Attributes_Secondary_ManaRegen;
	FGameplayTag Attributes_Secondary_MaxHealth;
	FGameplayTag Attributes_Secondary_MaxMana;
	
#pragma endregion

#pragma region Resistance Attributes
	
	FGameplayTag Attributes_Resistance_Fire;
	FGameplayTag Attributes_Resistance_Lightning;
	FGameplayTag Attributes_Resistance_Arcane;
	FGameplayTag Attributes_Resistance_Physical;
	FGameplayTag Attributes_Resistance_Ice;
	FGameplayTag Attributes_Resistance_Earth;
	
#pragma endregion

#pragma region Meta Attributes

	FGameplayTag Attributes_Meta_IncomingXP;
	
#pragma endregion

	
#pragma region Input Tags	

	FGameplayTag InputTag_LMB;
	FGameplayTag InputTag_RMB;
	FGameplayTag InputTag_1;
	FGameplayTag InputTag_2;
	FGameplayTag InputTag_3;
	FGameplayTag InputTag_4;
	FGameplayTag InputTag_Passive_1;
	FGameplayTag InputTag_Passive_2;
	
#pragma endregion

#pragma region Damage Types Tags
	
	FGameplayTag Damage;
	FGameplayTag Damage_Fire;
	FGameplayTag Damage_Lightning;
	FGameplayTag Damage_Arcane;
	FGameplayTag Damage_Physical;
	FGameplayTag Damage_Ice;
	FGameplayTag Damage_Earth;

#pragma endregion

#pragma region Debuffs

	FGameplayTag Debuff_Burn;
	FGameplayTag Debuff_Stun;
	FGameplayTag Debuff_Arcane;
	FGameplayTag Debuff_Physical;
	FGameplayTag Debuff_Frozen;
	FGameplayTag Debuff_Earth;

	FGameplayTag Debuff_Chance;
	FGameplayTag Debuff_Damage;
	FGameplayTag Debuff_Duration;
	FGameplayTag Debuff_Frequency;

#pragma endregion	

#pragma region Abilities

	FGameplayTag Abilities_None;
	
	FGameplayTag Abilities_Attack;
	FGameplayTag Abilities_Summon;

	FGameplayTag Abilities_HitReact;

	FGameplayTag Abilities_Status_Locked;
	FGameplayTag Abilities_Status_Eligible;
	FGameplayTag Abilities_Status_Unlocked;
	FGameplayTag Abilities_Status_Equipped;

	FGameplayTag Abilities_Type_Attacking;
	FGameplayTag Abilities_Type_Passive;
	FGameplayTag Abilities_Type_None;
	
	FGameplayTag Abilities_Fire_FireBolt;
	FGameplayTag Abilities_Fire_FireBlast;
	FGameplayTag Abilities_Lightning_Shock;
	FGameplayTag Abilities_Arcane_ArcaneShards;


	FGameplayTag Abilities_Passive_HaloOfProtection;
	FGameplayTag Abilities_Passive_LifeSiphon;
	FGameplayTag Abilities_Passive_ManaSiphon;

#pragma endregion

#pragma region Abilities Cooldown

	FGameplayTag Cooldown_Fire_FireBolt;
	FGameplayTag Cooldown_Fire_FireBlast;
	FGameplayTag Cooldown_Lightning_Shock;
	FGameplayTag Cooldown_Arcane_ArcaneShards;


#pragma endregion

#pragma region Combat Sockets

	FGameplayTag CombatSocket_Weapon;
	FGameplayTag CombatSocket_RightHand;
	FGameplayTag CombatSocket_LeftHand;
	FGameplayTag CombatSocket_Additional;
	
#pragma endregion
	
#pragma region Montage Tags
	
	FGameplayTag Montage_Attack_1;	
	FGameplayTag Montage_Attack_2;	
	FGameplayTag Montage_Attack_3;	
	FGameplayTag Montage_Attack_4;
	FGameplayTag Montage_Attack_5;
	
#pragma endregion 
	
	TMap<FGameplayTag, FGameplayTag> DamageTypesToResistances;
	TMap<FGameplayTag, FGameplayTag> DamageTypesToDebuff;
	
	FGameplayTag Effects_HitReact;
	
#pragma region Block Tags
	
	FGameplayTag Player_Block_InputPressed;
	FGameplayTag Player_Block_InputHeld;
	FGameplayTag Player_Block_InputReleased;
	FGameplayTag Player_Block_CursorTrace;
	
#pragma endregion

	FGameplayTag GameplayCue_FireBlast;	
	
private:

  static FRPG_GameplayTags GameplayTags;
 
};
