// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "RPGAbilitySystemTypes.h"
#include "GameFramework/Actor.h"
#include "RPGProjectile.generated.h"

struct FGameplayEffectSpecHandle;
class UProjectileMovementComponent;
class USphereComponent;
class UNiagaraSystem;
class USoundBase;

UCLASS()
class MAGICWORLDRPG_API ARPGProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	ARPGProjectile();

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category="Projectile")
    TObjectPtr<UProjectileMovementComponent> ProjectileMovement;

	/*UPROPERTY(EditAnywhere,BlueprintReadWrite, meta = (ExposeOnSpawn = true), Category="Gameplay Effects")
	FGameplayEffectSpecHandle DamageSpecHandle;*/

	UPROPERTY(EditAnywhere,BlueprintReadWrite, meta = (ExposeOnSpawn = true), Category="Gameplay Effects")
	FDamageEffectParams DamageEffectParams;

	UPROPERTY()
	TObjectPtr<USceneComponent> HomingTargetSceneComponent;
	
protected:
	virtual void BeginPlay() override;
	virtual void Destroyed() override;

	UFUNCTION(BlueprintCallable)
	virtual void OnHit();
	
	UFUNCTION()
	virtual void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,const FHitResult& SweepResult);

	bool bHit = false;

	bool IsValidOverlap(AActor* OtherActor);
	
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	TObjectPtr<USphereComponent> Sphere;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Effects")
	TObjectPtr<UNiagaraSystem> ImpactEffect;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Effects")
	TObjectPtr<USoundBase> ImpactSound;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Effects")
	TObjectPtr<USoundBase> LoopingSound;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Effects")
	float ProjectileLifeSpan = 15.f;

	UPROPERTY()
	TObjectPtr<UAudioComponent> LoopingSoundComponent;

	void PlayImpactEffects() const;
};
