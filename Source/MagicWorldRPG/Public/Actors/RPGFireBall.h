// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "Actors/RPGProjectile.h"
#include "RPGFireBall.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API ARPGFireBall : public ARPGProjectile
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintImplementableEvent)
	void StartOutgoingTimeline();

	UPROPERTY(BlueprintReadOnly)
	TObjectPtr<AActor> ReturnActor;
	
	UPROPERTY(BlueprintReadWrite)
	FDamageEffectParams ExplosionEffectParams;
protected:
	virtual void BeginPlay() override;
	virtual void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	virtual void OnHit() override;
	
};
