// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RPGMagicCircle.generated.h"

class UDecalComponent;

UCLASS()
class MAGICWORLDRPG_API ARPGMagicCircle : public AActor
{
	GENERATED_BODY()
	
public:	
	ARPGMagicCircle();
	
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
    TObjectPtr<UDecalComponent> MagicCircleDecal;
protected:
	virtual void BeginPlay() override;
	
};
