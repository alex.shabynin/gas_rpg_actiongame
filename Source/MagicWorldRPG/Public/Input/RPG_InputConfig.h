// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Engine/DataAsset.h"
#include "RPG_InputConfig.generated.h"

/**
 * 
 */

USTRUCT(BlueprintType)
struct FRPGInputAction
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Input")
	const class UInputAction* InputAction = nullptr;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Input")
	FGameplayTag InputTag = FGameplayTag();
};

UCLASS()
class MAGICWORLDRPG_API URPG_InputConfig : public UDataAsset
{
	GENERATED_BODY()

public:

	const UInputAction* FindAbilityInputActionForTag(FGameplayTag& InputTag, bool bLogNotFound = false) const;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	TArray<FRPGInputAction> AbilityInputActions;
};
