// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "EnhancedInputComponent.h"
#include "RPG_InputConfig.h"
#include "RPG_InputComponent.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPG_InputComponent : public UEnhancedInputComponent
{
	GENERATED_BODY()

public:

	template <class UserClass, typename PressedFuncType, typename ReleasedFuncType, typename HoldFuncType>
	void BindAbilityActions(const URPG_InputConfig* InputConfig, UserClass* Object, PressedFuncType PressedFunc, ReleasedFuncType ReleasedFunc, HoldFuncType HoldFunc);
};

template <class UserClass, typename PressedFuncType, typename ReleasedFuncType, typename HoldFuncType>
void URPG_InputComponent::BindAbilityActions(const URPG_InputConfig* InputConfig, UserClass* Object, PressedFuncType PressedFunc, ReleasedFuncType ReleasedFunc, HoldFuncType HoldFunc)
{
	check(InputConfig);
	for(const FRPGInputAction& Action : InputConfig->AbilityInputActions)
	{
		if(Action.InputAction && Action.InputTag.IsValid())
		{
			if(PressedFunc)
			{
				BindAction(Action.InputAction,ETriggerEvent::Started,Object,PressedFunc,Action.InputTag);
			}
			if(ReleasedFunc)
			{
				BindAction(Action.InputAction,ETriggerEvent::Completed,Object,ReleasedFunc,Action.InputTag);
			}
			if(HoldFunc)
			{
				BindAction(Action.InputAction,ETriggerEvent::Triggered,Object,HoldFunc,Action.InputTag);
			}
		}
	}
}
