// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "RPG_MainHUD.generated.h"

class URPGSpellMenuWidgetController;
class URPGAttributeMenuWidgetController;
struct FRPGWidgetControllerParams;
class URPGOverlayWidgetController;
class URPGUserWidget;
class UAbilitySystemComponent;
class UAttributeSet;
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API ARPG_MainHUD : public AHUD
{
	GENERATED_BODY()

public:

	URPGOverlayWidgetController* GetOverlayWidgetController(const FRPGWidgetControllerParams& WCParams);

	URPGAttributeMenuWidgetController* GetAttributeMenuWidgetController(const FRPGWidgetControllerParams& WCParams);

	URPGSpellMenuWidgetController* GetSpellMenuWidgetController(const FRPGWidgetControllerParams& WCParams);
	
	void InitOverlay(APlayerController* PC, APlayerState* PS, UAbilitySystemComponent* ASC, UAttributeSet* AS);
	
protected:
	UPROPERTY()
    TObjectPtr<URPGUserWidget> RPGOverlayWidget;
	
	UPROPERTY(EditAnywhere)
	TSubclassOf<URPGUserWidget> OverlayWidgetClass;
	
	UPROPERTY()
	TObjectPtr<URPGOverlayWidgetController> OverlayWidgetController;

	UPROPERTY(EditAnywhere)
	TSubclassOf<URPGOverlayWidgetController> OverlayWidgetControllerClass;

	UPROPERTY()
	TObjectPtr<URPGAttributeMenuWidgetController> AttributeMenuWidgetController;

	UPROPERTY(EditAnywhere)
	TSubclassOf<URPGAttributeMenuWidgetController> AttributeMenuWidgetControllerClass;

	UPROPERTY()
	TObjectPtr<URPGSpellMenuWidgetController> SpellMenuWidgetController;

	UPROPERTY(EditAnywhere)
	TSubclassOf<URPGSpellMenuWidgetController> SpellMenuWidgetControllerClass;
};
