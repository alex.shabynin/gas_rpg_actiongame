// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "DamageTextWidgetComp.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API UDamageTextWidgetComp : public UWidgetComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent,BlueprintCallable)
	void SetDamageText(float Damage, bool bIsBlocked, bool bIsCritical);
};
