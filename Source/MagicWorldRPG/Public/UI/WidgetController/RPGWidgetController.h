// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
//#include "UObject/NoExportTypes.h"
#include "RPGWidgetController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerStatChangedSignature, int32, NewLevel);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAbilityInfoSignature,const FRPG_AbilityInfo&,Info);

class URPGAbilitySystemComponent;
class ARPGPlayerController;
class ARPG_PlayerState;
class UAttributeSet;
class UAbilitySystemComponent;
class URPGAttributeSet;
class URPGAbilityInfo;

/**
 * 
 */
USTRUCT(BlueprintType)
struct FRPGWidgetControllerParams
{
	GENERATED_BODY()

	FRPGWidgetControllerParams() {}
	FRPGWidgetControllerParams(APlayerController* PC, APlayerState* PS, UAbilitySystemComponent* ASC, UAttributeSet* AS)
	: PlayerController(PC), PlayerState(PS), AbilitySystemComponent(ASC), AttributeSet(AS) {}

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TObjectPtr<APlayerController> PlayerController = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TObjectPtr<APlayerState> PlayerState = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TObjectPtr<UAbilitySystemComponent> AbilitySystemComponent = nullptr;
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TObjectPtr<UAttributeSet> AttributeSet = nullptr;
};

UCLASS()
class MAGICWORLDRPG_API URPGWidgetController : public UObject
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void SetWidgetControllerParams(const FRPGWidgetControllerParams& WCParams);

	UFUNCTION(BlueprintCallable)
	virtual void BroadcastInitialValues();

	virtual void BindCallbacksToDependencies();
	
	void BroadcastAbilityInfo();
	
	UPROPERTY(BlueprintAssignable,Category="GAS|Messages")
	FAbilityInfoSignature AbilityInfoDelegate;

	UFUNCTION(BlueprintPure,BlueprintCallable)
	ARPGPlayerController* GetRPGPlayerController();

	UFUNCTION(BlueprintPure,BlueprintCallable)
	ARPG_PlayerState* GetRPGPlayerState();

	UFUNCTION(BlueprintPure,BlueprintCallable)
	URPGAbilitySystemComponent* GetRPG_ASC();

	UFUNCTION(BlueprintPure,BlueprintCallable)
	URPGAttributeSet* GetRPG_AS();
	
protected:
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Widget Data")
	TObjectPtr<URPGAbilityInfo> AbilityInfo;
	
	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<APlayerController> PlayerController;
	
	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<APlayerState> PlayerState;
	
	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<UAbilitySystemComponent> AbilitySystemComponent;
	
	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<UAttributeSet> AttributeSet;

	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<ARPGPlayerController> RPGPlayerController;

	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<ARPG_PlayerState> RPGPlayerState;

	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<URPGAbilitySystemComponent> RPGAbilitySystemComponent;

	UPROPERTY(BlueprintReadOnly,Category="WidgetController")
	TObjectPtr<URPGAttributeSet> RPGAttribSet;
	
};
