// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "UI/WidgetController/RPGWidgetController.h"
#include "RPGAttributeMenuWidgetController.generated.h"

/**
 * 
 */

struct FGameplayAttribute;
struct FGameplayTag;
struct FRPG_AttributeInfo;
class URPGAttributeInfo;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAttributeInfoSignature, const FRPG_AttributeInfo&, Info);

UCLASS(BlueprintType,Blueprintable)
class MAGICWORLDRPG_API URPGAttributeMenuWidgetController : public URPGWidgetController
{
	GENERATED_BODY()

public:

	virtual void BindCallbacksToDependencies() override;

	virtual void BroadcastInitialValues() override;

	UPROPERTY(BlueprintAssignable,Category="GAS|Attributes")
	FAttributeInfoSignature AttributeInfoDelegate;

	UPROPERTY(EditAnywhere,BlueprintReadOnly)
	TObjectPtr<URPGAttributeInfo> AttributeInfo;

	UPROPERTY(BlueprintAssignable, Category="GAS|Attributes")
	FOnPlayerStatChangedSignature AttributePointsChangedDelegate;

	UFUNCTION(BlueprintCallable)
	void UpgradeAttribute(const FGameplayTag& AttributeTag);

private:
	void BroadcastAttributeInfo(const FGameplayTag& AttributeTag, const FGameplayAttribute& Attribute) const;
};
