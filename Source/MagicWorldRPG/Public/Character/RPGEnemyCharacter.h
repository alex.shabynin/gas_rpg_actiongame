// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "Character/RPGCharacterBase.h"
#include "Interaction/RPG_TargetInterface.h"
#include "UI/WidgetController/RPGOverlayWidgetController.h"
//#include "AbilitySystem/Data/RPGCharacterClassInfo.h"
#include "RPGEnemyCharacter.generated.h"

class UWidgetComponent;
class UBehaviorTree;
class ARPG_AIController;
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API ARPGEnemyCharacter : public ARPGCharacterBase, public IRPG_TargetInterface
{
	GENERATED_BODY()

public:
	ARPGEnemyCharacter();

	virtual void PossessedBy(AController* NewController) override;

#pragma region Enemy Interface	
	virtual void HighlightActor() override;
	virtual void UnHighlightActor() override;
	virtual void SetCombatTarget_Implementation(AActor* InCombatTarget) override;
    virtual AActor* GetCombatTarget_Implementation() const override;
#pragma endregion 	

#pragma region Combat Interface
virtual int32 GetPlayerLevel_Implementation() override;
virtual void Die(const FVector& DeathImpulse) override;
virtual void ChangeShockState_Implementation() override;	
	
#pragma endregion 	
	
	UPROPERTY(BlueprintReadOnly,Category="EnemyProperties")
	bool bIsHighlighted;

	UPROPERTY(BlueprintAssignable)
	FOnAttributeChangedSignature OnHealthChanged;

	UPROPERTY(BlueprintAssignable)
	FOnAttributeChangedSignature OnMaxHealthChanged;

	void HitReactTagChanged(const FGameplayTag CallbackTag,int32 NewCount);

	UPROPERTY(BlueprintReadOnly,Category="Combat")
	bool bHitReacting = false;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Combat")
	float DeadLifeSpan = 5.f;

	UPROPERTY(BlueprintReadWrite,Category="Combat")
	TObjectPtr<AActor> CombatTarget;
	
protected:
	virtual void BeginPlay() override;
	void InitAbilityActorInfo();
	virtual void InitializeDefaultAttributes() override;
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Character Defaults")
	int32 Level = 1;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Widget")
	TObjectPtr<UWidgetComponent> HealthBarWidget;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="AI")
	TObjectPtr<UBehaviorTree> BehaviorTree;
		
	UPROPERTY()
	TObjectPtr<ARPG_AIController> RPGAIController;
};
