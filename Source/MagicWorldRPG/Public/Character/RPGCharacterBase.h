// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "AbilitySystem/Data/RPGCharacterClassInfo.h"
#include "Interaction/RPG_CombatInterface.h"
#include "RPGCharacterBase.generated.h"


class ARPGPlayerController;
class URPGPassiveNiagaraComponent;
struct FEffectProperties;
class URPGDebuffNiagaraComponent;
class UNiagaraSystem;
class UGameplayAbility;
class UGameplayEffect;
class UAttributeSet;
class UAbilitySystemComponent;
class UAnimMontage;

UCLASS(Abstract)
class MAGICWORLDRPG_API ARPGCharacterBase : public ACharacter, public IAbilitySystemInterface, public IRPG_CombatInterface
{
	GENERATED_BODY()

public:
	ARPGCharacterBase();
	virtual void Tick(float DeltaSeconds) override;
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	UAttributeSet* GetAttributeSet() const {return AttributeSet;}
	
#pragma region Combat Interface
    
   	virtual UAnimMontage* GetHitReactMontage_Implementation() override;
        	
    virtual void Die(const FVector& DeathImpulse) override;
    	
    virtual bool IsDead_Implementation() const override;
    
    virtual AActor* GetAvatar_Implementation() override;
    
    virtual FVector GetCombatSocketLocation_Implementation(const FGameplayTag& MontageTag) override;

	virtual TArray<FTaggedMontage> GetAttackMontages_Implementation() override;

	virtual UNiagaraSystem* GetTargetImpactEffect_Implementation() override;

	virtual FTaggedMontage GetTaggedMontageByTag_Implementation(const FGameplayTag& MontageTag) override;

	virtual int32 GetMinionCount_Implementation() override;

	virtual void SetMinionCount_Implementation(int32 Amount) override;

	virtual ECharacterClass GetCharacterClass_Implementation() override;

	virtual FOnASCRegistered& GetOnASCRegisteredDelegate() override;

	virtual FOnDeath& GetOnDeathDelegate() override;

	virtual FOnDamageSignature& GetOnDamageSignature() override;

	virtual void SetDebuffImpactEffects(const FGameplayTag& DebuffTag, const FEffectProperties& Props, const float InDebuffDuration) override; //Testing

	virtual USkeletalMeshComponent* GetWeapon_Implementation() override;

	virtual bool IsBeingShocked_Implementation() const override;

	virtual void SetIsBeingShocked_Implementation(bool bInShock) override;

	virtual void ChangeShockState_Implementation() override;

    
   #pragma endregion

	FOnASCRegistered OnAscRegisteredDelegate;
	FOnDamageSignature OnDamageDelegate;

	UPROPERTY(BlueprintAssignable)
	FOnDeath OnDeathDelegate;
	
	UFUNCTION(NetMulticast,Reliable)
	virtual void MulticastHandleDeath(const FVector& DeathImpulse);

	UPROPERTY(EditAnywhere,BlueprintReadOnly, Category="Combat")
	TArray<FTaggedMontage> AttackMontages;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly)
	TObjectPtr<URPGDebuffNiagaraComponent> BurnDebuffComponent;

	UPROPERTY(ReplicatedUsing=OnRep_Stunned,BlueprintReadWrite)
	bool bStunned = false;

	UPROPERTY(Replicated,BlueprintReadOnly)
	bool bIsBeingShocked = false;

	virtual void StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount);

	UFUNCTION(BlueprintImplementableEvent,BlueprintCallable)
	void StunStateChanged();

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Combat")
	float BaseWalkSpeed = 600.f;

	UFUNCTION()
	virtual void OnRep_Stunned();
	
protected:
	virtual void BeginPlay() override;

	virtual void InitAbilityActorInfo();

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
    bool bDead = false;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category= "Combat")
	TObjectPtr<USkeletalMeshComponent> Weapon;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category= "Combat")
	FName WeaponSocket = "WeaponHandSocket";

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category= "Combat")
	FName WeaponTipSocket;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category= "Combat")
	FName LeftHandSocket;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category= "Combat")
	FName RightHandSocket;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category= "Combat")
	FName AdditionalSocket;
	
	UPROPERTY(BlueprintReadWrite,Category="AbilitySystem")
	TObjectPtr<UAbilitySystemComponent> AbilitySystemComponent;
	
	UPROPERTY(BlueprintReadWrite,Category="AbilitySystem")
	TObjectPtr<UAttributeSet> AttributeSet;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Effects")
	UNiagaraSystem* TargetImpactEffect;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Effects")
	USoundBase* DeathSound;

	int32 MinionsCount = 0;
	

#pragma region Gameplay Effects - Attributes
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Attributes")
	TSubclassOf<UGameplayEffect> DefaultPrimaryAttributes;
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Attributes")
	TSubclassOf<UGameplayEffect> DefaultSecondaryAttributes;
	
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Attributes")
	TSubclassOf<UGameplayEffect> DefaultVitalAttributes;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Abilities")
	TArray<TSubclassOf<UGameplayAbility>> StartupAbilities;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Abilities")
	TArray<TSubclassOf<UGameplayAbility>> StartupPassiveAbilities;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category="Character Defaults")
	ECharacterClass CharacterClass = ECharacterClass::Warrior;
	
#pragma endregion

#pragma region Animation Montages
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Combat")
	TObjectPtr<UAnimMontage> HitReactMontage;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Combat")
	TObjectPtr<UAnimMontage> DeathMontage;
	
#pragma endregion

#pragma region Dissolve Effects

	void Dissolve();

	UFUNCTION(BlueprintImplementableEvent)
	void StartDissolveTimeline(UMaterialInstanceDynamic* DynamicMaterialInstance);

	UFUNCTION(BlueprintImplementableEvent)
	void StartWeaponDissolveTimeline(UMaterialInstanceDynamic* DynamicMaterialInstance);
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dissolve")
	TObjectPtr<UMaterialInstance> MeshDissolveMaterialInstance;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category="Dissolve")
	TObjectPtr<UMaterialInstance> WeaponDissolveMaterialInstance;

#pragma endregion 	
	
	void ApplyEffectToSelf(TSubclassOf<UGameplayEffect> GameplayEffectClass, float Level);
	
	virtual void InitializeDefaultAttributes();

	void AddCharacterAbilities();

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TObjectPtr<URPGPassiveNiagaraComponent> PassiveNiagaraComponent_1;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TObjectPtr<URPGPassiveNiagaraComponent> PassiveNiagaraComponent_2;
	
	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	TObjectPtr<URPGPassiveNiagaraComponent> PassiveNiagaraComponent_3;
	
	UPROPERTY(VisibleAnywhere)
	TObjectPtr<USceneComponent> NiagaraEffectAttachComponent;
	
};
