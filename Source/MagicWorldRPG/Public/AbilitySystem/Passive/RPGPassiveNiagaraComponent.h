// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "NiagaraComponent.h"
#include "RPGPassiveNiagaraComponent.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGPassiveNiagaraComponent : public UNiagaraComponent
{
	GENERATED_BODY()
public:
	URPGPassiveNiagaraComponent();

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag PassiveSpellTag;

protected:
	virtual void BeginPlay() override;
	void OnPassiveAbilityActivated(const FGameplayTag& AbilityTag, bool bActivate);
	
};
