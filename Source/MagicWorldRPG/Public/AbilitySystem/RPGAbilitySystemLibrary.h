// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Data/RPGCharacterClassInfo.h"
#include "RPGAbilitySystemLibrary.generated.h"

class URPGAbilityInfo;
struct FGameplayEffectContextHandle;
class UAbilitySystemComponent;
class URPGAttributeMenuWidgetController;
class URPGOverlayWidgetController;
class URPGSpellMenuWidgetController;
struct FRPGWidgetControllerParams;
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGAbilitySystemLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
#pragma region Widget Controller	
	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|WidgetController",meta =(DefaultToSelf ="WorldContextObject"))
	static bool MakeWidgetControllerParams(const UObject* WorldContextObject, FRPGWidgetControllerParams& OutWCParams, ARPG_MainHUD*& OutMainHUD);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|WidgetController",meta =(DefaultToSelf ="WorldContextObject"))
	static URPGOverlayWidgetController* GetOverlayWidgetController(const UObject* WorldContextObject);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|WidgetController",meta =(DefaultToSelf ="WorldContextObject"))
	static URPGAttributeMenuWidgetController* GetAttributeMenuWidgetController(const UObject* WorldContextObject);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|WidgetController",meta =(DefaultToSelf ="WorldContextObject"))
	static URPGSpellMenuWidgetController* GetSpellMenuWidgetController(const UObject* WorldContextObject);
#pragma endregion

#pragma region CharacterClassDefaults	
	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|CharacterClassDefaults")
	static void InitializeDefaultAttributes(const UObject* WorldContextObject, ECharacterClass CharacterClass,float Level, UAbilitySystemComponent* ASC);	

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|CharacterClassDefaults")
	static void GiveStartupAbilities(const UObject* WorldContextObject, UAbilitySystemComponent* ASC, ECharacterClass CharacterClass);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|CharacterClassDefaults")
	static URPGCharacterClassInfo* GetCharacterClassInfo(const UObject* WorldContextObject);
#pragma endregion

#pragma region GameplayEffects
	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static bool IsBlockedHit(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static bool IsCriticalHit(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static bool IsSuccessfulDebuff(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static float GetDebuffDamage(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static float GetDebuffDuration(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static float GetDebuffFrequency(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static FGameplayTag GetDamageType(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static FVector GetDeathImpulse(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static FVector GetKnockBackForce(const FGameplayEffectContextHandle& EffectContextHandle);

	/*Radial Damage Getters*/
	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static bool IsRadialDamage(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static float GetRadialDamageInnerRadius(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static float GetRadialDamageOuterRadius(const FGameplayEffectContextHandle& EffectContextHandle);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static FVector GetRadialDamageOrigin(const FGameplayEffectContextHandle& EffectContextHandle);
	/*End Block*/
	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetIsBlockedHit(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, bool bIsInBlockedHit);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetIsCriticalHit(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, bool bIsInCriticalHit);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetIsSuccessfulDebuff(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, bool bIsInSuccessfullDebuff);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetDebuffDamage(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, float InDebuffDamage);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetDebuffDuration(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, float InDebuffDuration);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetDebuffFrequency(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, float InDebuffFrequency);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetDamageType(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, const FGameplayTag& InDamageType);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetDeathImpulse(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, const FVector& InDeathImpulse);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetKnockBackForce(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, const FVector& InKnockBackForce);
	
	/*Radial Damage Setters*/
	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetIsRadialDamage(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, bool bInIsRadialDamage);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetRadialInnerRadius(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, float InRadialInnerRadius);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetRadialOuterRadius(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, float InRadialOuterRadius);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayEffects")
	static void SetRadialOrigin(UPARAM(ref) FGameplayEffectContextHandle& EffectContextHandle, const FVector& InRadialOrigin);
	/*End Block*/
#pragma endregion

#pragma region GameplayMechanics	
	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static void GetLivePlayersWithinRadius(const UObject* WorldContextObject, TArray<AActor*>& OutOverlappingActors,const TArray<AActor*>& ActorsToIgnore, float Radius,const FVector& SphereOrigin);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static void GetClosestTargets(int32 MaxTargets, const TArray<AActor*>& Actors, TArray<AActor*>& OutClosestTargets, const FVector& Origin);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static bool IsNotFriendlyActor(AActor* FirstActor, AActor* SecondActor);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static int32 GetXPRewardForEnemyClass(UObject* WorldContextObject,ECharacterClass CharacterClass, int32 Level);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static URPGAbilityInfo* GetAbilityInfo(UObject* WorldContextObject);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static FGameplayEffectContextHandle ApplyDamageEffect(const FDamageEffectParams& DamageEffectParams);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static TArray<FRotator> EvenlySpacedRotators(const FVector& Forward, const FVector& Axis, float Spread, int32 NumRotators);

	UFUNCTION(BlueprintPure,Category="RPGAbilitySystemLibrary|GameplayMechanics")
	static TArray<FVector> EvenlyRotatedVectors(const FVector& Forward, const FVector& Axis, float Spread, int32 NumVectors);
#pragma endregion

#pragma region Damage Effect Params

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|DamageEffects")
	static void SetIsRadialDamageEffectParams(UPARAM(ref) FDamageEffectParams& DamageEffectParams, bool bIsRadial, float InnerRadius, float OuterRadius, FVector Origin);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|DamageEffects")
	static void SetKnockbackDirection(UPARAM(ref) FDamageEffectParams& DamageEffectParams, FVector KnockbackDirection, float Magnitude = 0.f);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|DamageEffects")
	static void SetDeathImpulseDirection(UPARAM(ref) FDamageEffectParams& DamageEffectParams, FVector DeathImpulseDirection,float Magnitude = 0.f);

	UFUNCTION(BlueprintCallable,Category="RPGAbilitySystemLibrary|DamageEffects")
	static void SetEffectParamsASC(UPARAM(ref) FDamageEffectParams& DamageEffectParams, UAbilitySystemComponent* InASC);	
	
#pragma endregion
	
};
