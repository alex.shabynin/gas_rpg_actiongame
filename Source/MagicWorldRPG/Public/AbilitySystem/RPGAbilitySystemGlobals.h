// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemGlobals.h"
#include "RPGAbilitySystemGlobals.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGAbilitySystemGlobals : public UAbilitySystemGlobals
{
	GENERATED_BODY()

public:
	
	virtual FGameplayEffectContext* AllocGameplayEffectContext() const override;
};
