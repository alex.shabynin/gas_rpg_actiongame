// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Engine/DataAsset.h"
#include "RPGAttributeInfo.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FRPG_AttributeInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FGameplayTag AttributeTag;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FText AttributeName;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FText AttributeDescription;

	UPROPERTY(BlueprintReadOnly)
	float AttributeValue = 0.f;
};

UCLASS()
class MAGICWORLDRPG_API URPGAttributeInfo : public UDataAsset
{
	GENERATED_BODY()

public:

	FRPG_AttributeInfo FindAttributeInfoForTag(const FGameplayTag& AttributeTag, bool bLogNotFound = false) const;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	TArray<FRPG_AttributeInfo> AttributeInformation;
	
};
