// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "NiagaraComponent.h"
#include "RPGDebuffNiagaraComponent.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGDebuffNiagaraComponent : public UNiagaraComponent
{
	GENERATED_BODY()
public:
	URPGDebuffNiagaraComponent();

	UPROPERTY(EditDefaultsOnly)
	FGameplayTag DebuffTag;

	virtual void BeginPlay() override;
	void DebuffTagChanged(const FGameplayTag CallbackTag, int32 NewCount);
	UFUNCTION()
	void OnOwnerDeath(AActor* DeadActor);
	
};
