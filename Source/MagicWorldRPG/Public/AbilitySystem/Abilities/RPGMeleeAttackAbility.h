// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Abilities/RPGDamageGameplayAbility.h"
#include "RPGMeleeAttackAbility.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGMeleeAttackAbility : public URPGDamageGameplayAbility
{
	GENERATED_BODY()
	
};
