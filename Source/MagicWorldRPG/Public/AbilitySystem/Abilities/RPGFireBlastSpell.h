// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Abilities/RPGDamageGameplayAbility.h"
#include "RPGFireBlastSpell.generated.h"

class ARPGFireBall;
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGFireBlastSpell : public URPGDamageGameplayAbility
{
	GENERATED_BODY()

public:

	virtual FString GetDescription(int32 Level) override;
	virtual FString GetNextLevelDescription(int32 Level) override;

	UFUNCTION(BlueprintCallable)
	TArray<ARPGFireBall*> SpawnFireBalls();

protected:
	
	UPROPERTY(EditDefaultsOnly, Category = "FireBlast")
	int32 NumFireBalls = 12;

	UPROPERTY(EditDefaultsOnly, Category="FireBlast")
	TSubclassOf<ARPGFireBall> FireballProjectileClass;
};
