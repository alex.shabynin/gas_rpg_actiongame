// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Abilities/RPGBeamSpell.h"
#include "RPGElectroShockSpell.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGElectroShockSpell : public URPGBeamSpell
{
	GENERATED_BODY()
	
public:
	virtual FString GetDescription(int32 Level) override;
	virtual FString GetNextLevelDescription(int32 Level) override;
};
