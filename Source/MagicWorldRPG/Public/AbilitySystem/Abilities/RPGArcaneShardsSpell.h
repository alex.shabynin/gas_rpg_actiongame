// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystem/Abilities/RPGDamageGameplayAbility.h"
#include "RPGArcaneShardsSpell.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGArcaneShardsSpell : public URPGDamageGameplayAbility
{
	GENERATED_BODY()

public:

	virtual FString GetDescription(int32 Level) override;
	virtual FString GetNextLevelDescription(int32 Level) override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly,Category = "ArcaneShards")
	int32 MaxNumArcaneShards = 11;
};
