// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "MMC_MaxHealth.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API UMMC_MaxHealth : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	UMMC_MaxHealth();

	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;

protected:
	
	UPROPERTY()
	FGameplayEffectAttributeCaptureDefinition VigorDef;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Values")
	float Coefficient = 2.5;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Values")
	float PreMAV = 10.f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Values")
	float PostMAV = 80.f;
};
