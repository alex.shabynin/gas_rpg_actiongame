// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayModMagnitudeCalculation.h"
#include "MMC_MaxMana.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API UMMC_MaxMana : public UGameplayModMagnitudeCalculation
{
	GENERATED_BODY()

public:
	UMMC_MaxMana();
	
	virtual float CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const override;
	
protected:
	UPROPERTY()
	FGameplayEffectAttributeCaptureDefinition IntelligenceDef;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Values")
	float Coefficient = 2;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Values")
	float PreMAV = 10.f;
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Values")
	float PostMAV = 50.f;
};
