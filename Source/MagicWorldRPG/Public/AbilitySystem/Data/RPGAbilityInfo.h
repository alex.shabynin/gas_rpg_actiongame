// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "Engine/DataAsset.h"
#include "RPGAbilityInfo.generated.h"


class UNiagaraSystem;
class UGameplayAbility;

USTRUCT(BlueprintType)
struct FRPG_AbilityInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FGameplayTag AbilityTag = FGameplayTag();

	UPROPERTY(BlueprintReadOnly)
	FGameplayTag InputTag = FGameplayTag();

	UPROPERTY(BlueprintReadOnly)
	FGameplayTag StatusTag = FGameplayTag();

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FGameplayTag CooldownTag = FGameplayTag();

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FGameplayTag AbilityType = FGameplayTag();

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	FGameplayTag DebuffType = FGameplayTag(); //Testing

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	UNiagaraSystem* DebuffEffect = nullptr; //Testing
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
    int32 LevelRequirements = 1;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	TSubclassOf<UGameplayAbility> Ability;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	TObjectPtr<const UTexture2D> AbilityIcon = nullptr;
	
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	TObjectPtr<const UMaterialInterface> BackgroundImage = nullptr;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	TObjectPtr<const UMaterialInterface> SquareBackgroundImage = nullptr;
	
};
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGAbilityInfo : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="Ability Information")
	TArray<FRPG_AbilityInfo> AbilityInformation;

	UFUNCTION(BlueprintPure,BlueprintCallable)
	FRPG_AbilityInfo FindAbilityInfoByTag(const FGameplayTag& AbilityTag, bool bLogNotFound = false) const;
	
};
