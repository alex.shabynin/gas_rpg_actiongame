// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "RPGLevelUpInfo.generated.h"

USTRUCT(BlueprintType)
struct FRPG_LevelUpInfo
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	int32 LevelUpRequirements = 0;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	int32 AttributePointsReward = 1;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly)
	int32 SkillPointsReward = 1;
};
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPGLevelUpInfo : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category="")
	TArray<FRPG_LevelUpInfo> LevelUpInformation;

	int32 FindLevelForXP(int32 XP) const;
	
};
