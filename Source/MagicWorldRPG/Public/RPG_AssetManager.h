// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "Engine/AssetManager.h"
#include "RPG_AssetManager.generated.h"

/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API URPG_AssetManager : public UAssetManager
{
	GENERATED_BODY()

public:

	static URPG_AssetManager& Get();

protected:

	virtual void StartInitialLoading() override;
	
};
