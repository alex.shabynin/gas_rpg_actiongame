// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "RPG_TargetInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class URPG_TargetInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class MAGICWORLDRPG_API IRPG_TargetInterface
{
	GENERATED_BODY()

public:

	virtual void HighlightActor() = 0;
	virtual void UnHighlightActor() = 0;

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
	void SetCombatTarget(AActor* InCombatTarget);

	UFUNCTION(BlueprintCallable,BlueprintNativeEvent)
	AActor* GetCombatTarget() const;
};
