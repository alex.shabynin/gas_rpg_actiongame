// Copyright Alex Shabynin

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "RPG_AIController.generated.h"

class UBlackboardComponent;
class UBehaviorTreeComponent;
/**
 * 
 */
UCLASS()
class MAGICWORLDRPG_API ARPG_AIController : public AAIController
{
	GENERATED_BODY()

public:

	ARPG_AIController();

protected:	
	
	UPROPERTY()
	TObjectPtr<UBehaviorTreeComponent> BehaviorTreeComponent;
	
};
