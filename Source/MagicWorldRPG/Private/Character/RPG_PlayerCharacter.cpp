// Copyright Alex Shabynin


#include "Character/RPG_PlayerCharacter.h"

#include "AbilitySystemComponent.h"
#include "NiagaraComponent.h"
#include "RPG_GameplayTags.h"
#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "AbilitySystem/Data/RPGLevelUpInfo.h"
#include "AbilitySystem/Debuff/RPGDebuffNiagaraComponent.h"
#include "Core/RPGPlayerController.h"
#include "Core/RPG_PlayerState.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "UI/HUD/RPG_MainHUD.h"

ARPG_PlayerCharacter::ARPG_PlayerCharacter()
{
	LevelUpNiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>("NiagaraEffectComp");
	LevelUpNiagaraComponent->SetupAttachment(GetRootComponent());
	LevelUpNiagaraComponent->bAutoActivate = false;
	
	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.f,400.f,0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	CharacterClass = ECharacterClass::Mage;
}

void ARPG_PlayerCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	//Init Ability actor info for the server

	InitAbilityActorInfo();
	AddCharacterAbilities();
}

void ARPG_PlayerCharacter::OnRep_PlayerState()
{
	Super::OnRep_PlayerState();

	//Init Ability actor info for the client
	InitAbilityActorInfo();

}

void ARPG_PlayerCharacter::InitAbilityActorInfo()
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState)
	RPGPlayerState->GetAbilitySystemComponent()->InitAbilityActorInfo(RPGPlayerState,this);
	Cast<URPGAbilitySystemComponent>(RPGPlayerState->GetAbilitySystemComponent())->AbilityActorInfoSet();
	AbilitySystemComponent = RPGPlayerState->GetAbilitySystemComponent();
	AttributeSet = RPGPlayerState->GetAttributeSet();
	OnAscRegisteredDelegate.Broadcast(AbilitySystemComponent);
	AbilitySystemComponent->RegisterGameplayTagEvent(FRPG_GameplayTags::Get().Debuff_Stun,EGameplayTagEventType::NewOrRemoved).AddUObject(this,&ARPG_PlayerCharacter::StunTagChanged);
	
	if(ARPGPlayerController* RPGPlayerController = Cast<ARPGPlayerController>(GetController()))
	{
		ARPG_MainHUD* MainHUD = Cast<ARPG_MainHUD>(RPGPlayerController->GetHUD());
		MainHUD->InitOverlay(RPGPlayerController,RPGPlayerState,AbilitySystemComponent,AttributeSet);
	}
	InitializeDefaultAttributes();
}

/* Combat Interface*/

int32 ARPG_PlayerCharacter::GetPlayerLevel_Implementation()
{
	const ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);

	return RPGPlayerState->GetPlayerLevel();
}
/* end Combat Interface*/

/* Player Interface*/
void ARPG_PlayerCharacter::AddToXP_Implementation(int32 InXP)
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);

	RPGPlayerState->AddToXP(InXP);
}

void ARPG_PlayerCharacter::LevelUp_Implementation()
{
	MulticastLevelUpParticles();
}

void ARPG_PlayerCharacter::MulticastLevelUpParticles_Implementation()
{
	if(IsValid(LevelUpNiagaraComponent))
	{
		LevelUpNiagaraComponent->Activate(true);
	}
	
}

int32 ARPG_PlayerCharacter::GetXP_Implementation()
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	
	return RPGPlayerState->GetXP();
}

int32 ARPG_PlayerCharacter::FindLevelForXP_Implementation(int32 InXP)
{
	const ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	return RPGPlayerState->LevelUpInfo->FindLevelForXP(InXP);
	
}

int32 ARPG_PlayerCharacter::GetAttributePointsReward_Implementation(int32 Level) const
{
	const ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	return RPGPlayerState->LevelUpInfo->LevelUpInformation[Level].AttributePointsReward;
}

int32 ARPG_PlayerCharacter::GetSpellPointsReward_Implementation(int32 Level) const
{
	const ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	return RPGPlayerState->LevelUpInfo->LevelUpInformation[Level].SkillPointsReward;
}

void ARPG_PlayerCharacter::AddPlayerLevel_Implementation(int32 InLevel)
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	RPGPlayerState->AddToLevel(InLevel);

	if(URPGAbilitySystemComponent* ASC = Cast<URPGAbilitySystemComponent>(GetAbilitySystemComponent()))
	{
		ASC->UpdateAbilityStatuses(RPGPlayerState->GetPlayerLevel());
	}
}

void ARPG_PlayerCharacter::AddAttributePoints_Implementation(int32 InAttributePoints)
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	RPGPlayerState->AddAttributePoints(InAttributePoints);
}

void ARPG_PlayerCharacter::AddSpellPoints_Implementation(int32 InSpellPoints)
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	RPGPlayerState->AddSpellPoints(InSpellPoints);
}

int32 ARPG_PlayerCharacter::GetAttributePoints_Implementation() const
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	return RPGPlayerState->GetAttributePoints();
}

int32 ARPG_PlayerCharacter::GetSpellPoints_Implementation() const
{
	ARPG_PlayerState* RPGPlayerState = GetPlayerState<ARPG_PlayerState>();
	check(RPGPlayerState);
	return RPGPlayerState->GetSpellPoints();
}

void ARPG_PlayerCharacter::ShowMagicCircle_Implementation(UMaterialInterface* DecalMaterial)
{
	if(ARPGPlayerController* RPGPlayerController = Cast<ARPGPlayerController>(GetController()))
	{
		RPGPlayerController->ShowMagicCircle(DecalMaterial);
        RPGPlayerController->bShowMouseCursor = false;
	}
	
}

void ARPG_PlayerCharacter::HideMagicCircle_Implementation()
{
	if(ARPGPlayerController* RPGPlayerController = Cast<ARPGPlayerController>(GetController()))
	{
		RPGPlayerController->HideMagicCircle();
		RPGPlayerController->bShowMouseCursor = true;
	}
}


void ARPG_PlayerCharacter::OnRep_Stunned()
{
	if(URPGAbilitySystemComponent* RPG_AbilitySystemComponent = Cast<URPGAbilitySystemComponent>(AbilitySystemComponent))
	{
		FGameplayTagContainer BlockedTags;
		BlockedTags.AddTag(FRPG_GameplayTags::Get().Player_Block_CursorTrace);
		BlockedTags.AddTag(FRPG_GameplayTags::Get().Player_Block_InputPressed);
		BlockedTags.AddTag(FRPG_GameplayTags::Get().Player_Block_InputHeld);
		BlockedTags.AddTag(FRPG_GameplayTags::Get().Player_Block_InputReleased);
		if(bStunned)
		{
			RPG_AbilitySystemComponent->AddLooseGameplayTags(BlockedTags);
		}
		else
		{
			RPG_AbilitySystemComponent->RemoveLooseGameplayTags(BlockedTags);
		}
	}
	
}





