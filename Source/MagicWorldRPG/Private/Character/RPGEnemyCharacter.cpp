// Copyright Alex Shabynin


#include "Character/RPGEnemyCharacter.h"

#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "AbilitySystem/RPGAbilitySystemLibrary.h"
#include "AbilitySystem/RPGAttributeSet.h"
#include "Components/WidgetComponent.h"
#include "UI/Widget/RPGUserWidget.h"
#include "RPG_GameplayTags.h"
#include "AI/RPG_AIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

ARPGEnemyCharacter::ARPGEnemyCharacter()
{
	GetMesh()->SetCollisionResponseToChannel(ECC_Visibility,ECR_Block);
	GetMesh()->SetRenderCustomDepth(false);
	GetMesh()->SetCustomDepthStencilValue(250);
	Weapon->SetRenderCustomDepth(false);
	Weapon->SetCustomDepthStencilValue(250);
	
	AbilitySystemComponent = CreateDefaultSubobject<URPGAbilitySystemComponent>("AbilitySystemComponent");
	AbilitySystemComponent->SetIsReplicated(true);
	AbilitySystemComponent->SetReplicationMode(EGameplayEffectReplicationMode::Minimal);

	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	
	AttributeSet = CreateDefaultSubobject<URPGAttributeSet>("AttributeSet");

	HealthBarWidget = CreateDefaultSubobject<UWidgetComponent>("Health Bar Widget");
	HealthBarWidget->SetupAttachment(GetRootComponent());
	
	BaseWalkSpeed = 250.f;
}

void ARPGEnemyCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if(!HasAuthority()) return;
	
	RPGAIController = Cast<ARPG_AIController>(NewController);

	//Initializing BlackBoard and Run BehaviorTree from AIController
	RPGAIController->GetBlackboardComponent()->InitializeBlackboard(*BehaviorTree->BlackboardAsset);
	RPGAIController->RunBehaviorTree(BehaviorTree);
	RPGAIController->GetBlackboardComponent()->SetValueAsBool(FName("HitReacting"),false);
	RPGAIController->GetBlackboardComponent()->SetValueAsBool(FName("RangedAttacker"),CharacterClass != ECharacterClass::Warrior);
	
}


void ARPGEnemyCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetCharacterMovement()->MaxWalkSpeed = BaseWalkSpeed;
	InitAbilityActorInfo();

	URPGAbilitySystemComponent* ASC = CastChecked<URPGAbilitySystemComponent>(GetAbilitySystemComponent());

	if(HasAuthority())
	{
		ASC->AddCharacterAbilities(StartupAbilities);
		/*
		 *Временно заменил применение Abilities из класса врага из массива StartupAbilities, до этого данные брали из DataAsset - CharacterClassInfo
		 *Если в CharacterClassInfo поле StartupAbilities не заполнено, берем их из того же поля, но у класса врага
		 */
		URPGAbilitySystemLibrary::GiveStartupAbilities(this,AbilitySystemComponent, CharacterClass);
		
	}
	
	if(URPGUserWidget* RPGUserWidget = Cast<URPGUserWidget>(HealthBarWidget->GetUserWidgetObject()))
	{
		RPGUserWidget->SetWidgetController(this);
	}
	
	const URPGAttributeSet* RPGAttributeSet = CastChecked<URPGAttributeSet>(AttributeSet);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(RPGAttributeSet->GetHealthAttribute()).AddLambda(
	[this] (const FOnAttributeChangeData& Data)
	{
		OnHealthChanged.Broadcast(Data.NewValue);	
	}
	);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(RPGAttributeSet->GetMaxHealthAttribute()).AddLambda(
	[this] (const FOnAttributeChangeData& Data)
	{
		OnMaxHealthChanged.Broadcast(Data.NewValue);	
	}
	);
	
	AbilitySystemComponent->RegisterGameplayTagEvent(FRPG_GameplayTags::Get().Effects_HitReact).AddUObject(
		this, &ARPGEnemyCharacter::HitReactTagChanged);
	
	
	OnHealthChanged.Broadcast(RPGAttributeSet->GetHealth());
	OnMaxHealthChanged.Broadcast(RPGAttributeSet->GetMaxHealth());
}

void ARPGEnemyCharacter::HitReactTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	bHitReacting = NewCount >0;
	GetCharacterMovement()->MaxWalkSpeed = bHitReacting ? 0.f : BaseWalkSpeed;
	
	if(RPGAIController && RPGAIController->GetBlackboardComponent())
	{
		RPGAIController->GetBlackboardComponent()->SetValueAsBool(FName("HitReacting"),bHitReacting);
	}
	
	
}

void ARPGEnemyCharacter::HighlightActor()
{
	bIsHighlighted = true;
	GetMesh()->SetRenderCustomDepth(true);
	Weapon->SetRenderCustomDepth(true);

	//Debug::Print(TEXT("Highlighted"),2.f);

}

void ARPGEnemyCharacter::UnHighlightActor()
{
	bIsHighlighted = false;
	GetMesh()->SetRenderCustomDepth(false);
	Weapon->SetRenderCustomDepth(false);

	//Debug::Print(TEXT("UnHighlighted"),2.f);
}

void ARPGEnemyCharacter::SetCombatTarget_Implementation(AActor* InCombatTarget)
{
	CombatTarget = InCombatTarget;
}

AActor* ARPGEnemyCharacter::GetCombatTarget_Implementation() const
{
	return CombatTarget;
}

void ARPGEnemyCharacter::InitAbilityActorInfo()
{
	AbilitySystemComponent->InitAbilityActorInfo(this,this);
	Cast<URPGAbilitySystemComponent>(AbilitySystemComponent)->AbilityActorInfoSet();
	AbilitySystemComponent->RegisterGameplayTagEvent(FRPG_GameplayTags::Get().Debuff_Stun,EGameplayTagEventType::NewOrRemoved).AddUObject(this, &ARPGEnemyCharacter::StunTagChanged);
	if(HasAuthority())
	{
		InitializeDefaultAttributes();
	}
	OnAscRegisteredDelegate.Broadcast(AbilitySystemComponent);
}

void ARPGEnemyCharacter::InitializeDefaultAttributes()
{
	URPGAbilitySystemLibrary::InitializeDefaultAttributes(this,CharacterClass,Level,AbilitySystemComponent);
}

int32 ARPGEnemyCharacter::GetPlayerLevel_Implementation()
{
	return Level;
}

void ARPGEnemyCharacter::Die(const FVector& DeathImpulse)
{
	if(RPGAIController) RPGAIController->GetBlackboardComponent()->SetValueAsBool(FName("IsDead"),true);
	SetLifeSpan(DeadLifeSpan);
	Super::Die(DeathImpulse);
}

void ARPGEnemyCharacter::ChangeShockState_Implementation()
{
	Super::ChangeShockState_Implementation();
	
	RPGAIController->GetBlackboardComponent()->SetValueAsBool(FName("InShockLoop"),bIsBeingShocked);
	
}
