// Copyright Alex Shabynin


#include "Character/RPGCharacterBase.h"
#include "AbilitySystemComponent.h"
#include "RPG_GameplayTags.h"
#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "AbilitySystem/RPGAbilitySystemLibrary.h"
#include "AbilitySystem/RPGAttributeSet.h"
#include "AbilitySystem/Data/RPGAbilityInfo.h"
#include "AbilitySystem/Debuff/RPGDebuffNiagaraComponent.h"
#include "AbilitySystem/Passive/RPGPassiveNiagaraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Core/RPGPlayerController.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "MagicWorldRPG/MagicWorldRPG.h"
#include "Net/UnrealNetwork.h"

ARPGCharacterBase::ARPGCharacterBase()
{
	PrimaryActorTick.bCanEverTick = true;

	BurnDebuffComponent = CreateDefaultSubobject<URPGDebuffNiagaraComponent>("BurnDebuffEffect");
	BurnDebuffComponent->SetupAttachment(GetRootComponent());
	BurnDebuffComponent->DebuffTag = FRPG_GameplayTags::Get().Debuff_Burn;

	GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);
	GetCapsuleComponent()->SetGenerateOverlapEvents(false);
	GetMesh()->SetGenerateOverlapEvents(true);
	GetMesh()->SetCollisionResponseToChannel(ECC_Projectile,ECR_Overlap);
	GetMesh()->SetCollisionResponseToChannel(ECC_Camera,ECR_Ignore);

	Weapon = CreateDefaultSubobject<USkeletalMeshComponent>("Weapon");
	Weapon->SetupAttachment(GetMesh(), WeaponSocket);
	Weapon->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	NiagaraEffectAttachComponent = CreateDefaultSubobject<USceneComponent>("EffectAttachComponent");
	NiagaraEffectAttachComponent->SetupAttachment(GetRootComponent());
	
	PassiveNiagaraComponent_1 = CreateDefaultSubobject<URPGPassiveNiagaraComponent>("PassiveEffectComponent_1");
	PassiveNiagaraComponent_1->SetupAttachment(NiagaraEffectAttachComponent);
	
	PassiveNiagaraComponent_2 = CreateDefaultSubobject<URPGPassiveNiagaraComponent>("PassiveEffectComponent_2");
	PassiveNiagaraComponent_2->SetupAttachment(NiagaraEffectAttachComponent);
	
	PassiveNiagaraComponent_3 = CreateDefaultSubobject<URPGPassiveNiagaraComponent>("PassiveEffectComponent_3");
	PassiveNiagaraComponent_3->SetupAttachment(NiagaraEffectAttachComponent);
}

void ARPGCharacterBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	NiagaraEffectAttachComponent->SetWorldRotation(FRotator::ZeroRotator);
}

UAbilitySystemComponent* ARPGCharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

void ARPGCharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);
	DOREPLIFETIME(ARPGCharacterBase,bStunned);
	DOREPLIFETIME(ARPGCharacterBase,bIsBeingShocked);

}

float ARPGCharacterBase::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
	AActor* DamageCauser)
{
	const float DamageTaken = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	OnDamageDelegate.Broadcast(DamageTaken);
	return DamageTaken;
}

UAnimMontage* ARPGCharacterBase::GetHitReactMontage_Implementation()
{
	return HitReactMontage;
}


void ARPGCharacterBase::StunTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	bStunned = NewCount > 0;
	GetCharacterMovement()->MaxWalkSpeed = bStunned ? 0.f : BaseWalkSpeed;
}

void ARPGCharacterBase::InitAbilityActorInfo()
{
	
}

void ARPGCharacterBase::OnRep_Stunned()
{
	
}

void ARPGCharacterBase::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ARPGCharacterBase::IsDead_Implementation() const
{
	return bDead;
}

AActor* ARPGCharacterBase::GetAvatar_Implementation()
{
	return this;
}

void ARPGCharacterBase::Die(const FVector& DeathImpulse)
{
	Weapon->DetachFromComponent(FDetachmentTransformRules(EDetachmentRule::KeepWorld,true));
	if(BurnDebuffComponent->IsActive())
	{
		BurnDebuffComponent->Deactivate();
	}
	MulticastHandleDeath(DeathImpulse);
}

void ARPGCharacterBase::MulticastHandleDeath_Implementation(const FVector& DeathImpulse)
{
	Weapon->SetSimulatePhysics(true);
	Weapon->SetEnableGravity(true);
	Weapon->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	if(IsValid(Weapon))
	{
		Weapon->AddImpulse(DeathImpulse * 0.15f,NAME_None,true);
	}
	
	if(DeathMontage)
		{
			GetMesh()->GetAnimInstance()->Montage_Play(DeathMontage);
		}
	GetMesh()->SetSimulatePhysics(true);
	GetMesh()->SetEnableGravity(true);
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	GetMesh()->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
	GetMesh()->AddImpulse(DeathImpulse,NAME_None,true);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Dissolve();
	
	bDead = true;
	//BurnDebuffComponent->Deactivate();
	if(DeathSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this,DeathSound,GetActorLocation(),GetActorRotation());
	}
	OnDeathDelegate.Broadcast(this);
}

FVector ARPGCharacterBase::GetCombatSocketLocation_Implementation(const FGameplayTag& MontageTag)
{
	const FRPG_GameplayTags& GameplayTags = FRPG_GameplayTags::Get();
	if(MontageTag.MatchesTagExact(GameplayTags.CombatSocket_Weapon) && IsValid(Weapon))
	{
		return Weapon->GetSocketLocation(WeaponTipSocket);
	}
	if(MontageTag.MatchesTagExact(GameplayTags.CombatSocket_LeftHand))
	{
		return GetMesh()->GetSocketLocation(LeftHandSocket);
	}
	if(MontageTag.MatchesTagExact(GameplayTags.CombatSocket_RightHand))
	{
		return GetMesh()->GetSocketLocation(RightHandSocket);
	}
	if(MontageTag.MatchesTagExact(GameplayTags.CombatSocket_Additional))
	{
		return GetMesh()->GetSocketLocation(AdditionalSocket);
	}
	return FVector();
}

TArray<FTaggedMontage> ARPGCharacterBase::GetAttackMontages_Implementation()
{
	return AttackMontages;
}

UNiagaraSystem* ARPGCharacterBase::GetTargetImpactEffect_Implementation()
{
	return TargetImpactEffect;
}

FTaggedMontage ARPGCharacterBase::GetTaggedMontageByTag_Implementation(const FGameplayTag& MontageTag)
{
	for(FTaggedMontage TaggedMontage : AttackMontages)
	{
		if(TaggedMontage.MontageTag == MontageTag)
		{
			return TaggedMontage;
		}
	}
	return FTaggedMontage();
}

int32 ARPGCharacterBase::GetMinionCount_Implementation()
{
	return MinionsCount;
}

void ARPGCharacterBase::SetMinionCount_Implementation(int32 Amount)
{
	MinionsCount += Amount;
}

ECharacterClass ARPGCharacterBase::GetCharacterClass_Implementation()
{
	return CharacterClass;
}

FOnASCRegistered& ARPGCharacterBase::GetOnASCRegisteredDelegate()
{
	return OnAscRegisteredDelegate;
}

FOnDeath& ARPGCharacterBase::GetOnDeathDelegate()
{
	return OnDeathDelegate;
}

FOnDamageSignature& ARPGCharacterBase::GetOnDamageSignature()
{
	return OnDamageDelegate;
}

void ARPGCharacterBase::SetDebuffImpactEffects(const FGameplayTag& DebuffTag,
                                               const FEffectProperties& Props, const float InDebuffDuration)
{
	URPGAbilityInfo* AbilityInfo = URPGAbilitySystemLibrary::GetAbilityInfo(Props.SourceAvatarActor);
	for(const FRPG_AbilityInfo& Info : AbilityInfo->AbilityInformation)
	{
		if(DebuffTag.MatchesTagExact(Info.DebuffType))
		{
			BurnDebuffComponent->SetAsset(Info.DebuffEffect);
		}
		if(DebuffTag.MatchesTagExact(FRPG_GameplayTags::Get().Debuff_Stun))
		{
			bStunned = true;
			StunStateChanged();
		}
	}
			
	FTimerDelegate EffectTimer;
	FTimerHandle EffectTimerHandle;
	if(EffectTimerHandle.IsValid())
	{
		EffectTimerHandle.Invalidate();
	}

	if(!bDead)
	{
		BurnDebuffComponent->Activate();
	}
			
			
	EffectTimer.BindLambda([this]()
	{
		BurnDebuffComponent->Deactivate();
		Tags.Remove(FName("Immune")); //Testing - reactivate HitReacting by removing tag, when debuff deactivated
		bStunned = false;
		StunStateChanged();
	});
			
	GetWorld()->GetTimerManager().SetTimer(EffectTimerHandle,EffectTimer,InDebuffDuration,false);
}

USkeletalMeshComponent* ARPGCharacterBase::GetWeapon_Implementation()
{
	return Weapon;
}

bool ARPGCharacterBase::IsBeingShocked_Implementation() const
{
	return bIsBeingShocked;
}

void ARPGCharacterBase::SetIsBeingShocked_Implementation(bool bInShock)
{
	bIsBeingShocked = bInShock;
}

void ARPGCharacterBase::ChangeShockState_Implementation()
{
	
}

void ARPGCharacterBase::Dissolve()
{
	if(IsValid(MeshDissolveMaterialInstance))
	{
		UMaterialInstanceDynamic* DynamicInst = UMaterialInstanceDynamic::Create(MeshDissolveMaterialInstance,this);
		GetMesh()->SetMaterial(0,DynamicInst);
		StartDissolveTimeline(DynamicInst);
	}
	if(IsValid(WeaponDissolveMaterialInstance))
	{
		UMaterialInstanceDynamic* DynamicInst = UMaterialInstanceDynamic::Create(WeaponDissolveMaterialInstance,this);
		Weapon->SetMaterial(0,DynamicInst);
		StartWeaponDissolveTimeline(DynamicInst);
	}
}

void ARPGCharacterBase::ApplyEffectToSelf(TSubclassOf<UGameplayEffect> GameplayEffectClass, float Level)
{
	check(IsValid(GetAbilitySystemComponent()))
	check(GameplayEffectClass)
	FGameplayEffectContextHandle ContextHandle = GetAbilitySystemComponent()->MakeEffectContext();
	ContextHandle.AddSourceObject(this);
	const FGameplayEffectSpecHandle SpecHandle = GetAbilitySystemComponent()->MakeOutgoingSpec(GameplayEffectClass,Level,ContextHandle);
	GetAbilitySystemComponent()->ApplyGameplayEffectSpecToTarget(*SpecHandle.Data.Get(),GetAbilitySystemComponent());
}

void ARPGCharacterBase::InitializeDefaultAttributes()
{
	ApplyEffectToSelf(DefaultPrimaryAttributes, 1.f);
	ApplyEffectToSelf(DefaultSecondaryAttributes, 1.f);
	ApplyEffectToSelf(DefaultVitalAttributes, 1.f);

}

void ARPGCharacterBase::AddCharacterAbilities()
{
	URPGAbilitySystemComponent* ASC = CastChecked<URPGAbilitySystemComponent>(GetAbilitySystemComponent());
	if(!HasAuthority()) return;
	
	ASC->AddCharacterAbilities(StartupAbilities);
	ASC->AddCharacterPassiveAbilities(StartupPassiveAbilities);
}


