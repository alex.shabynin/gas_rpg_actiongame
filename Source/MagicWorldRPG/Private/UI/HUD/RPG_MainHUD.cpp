// Copyright Alex Shabynin


#include "UI/HUD/RPG_MainHUD.h"

#include "UI/Widget/RPGUserWidget.h"
#include "UI/WidgetController/RPGAttributeMenuWidgetController.h"
#include "UI/WidgetController/RPGOverlayWidgetController.h"
#include "UI/WidgetController/RPGSpellMenuWidgetController.h"

URPGOverlayWidgetController* ARPG_MainHUD::GetOverlayWidgetController(const FRPGWidgetControllerParams& WCParams)
{
	if(OverlayWidgetController == nullptr)
	{
		OverlayWidgetController = NewObject<URPGOverlayWidgetController>(this, OverlayWidgetControllerClass);
		OverlayWidgetController->SetWidgetControllerParams(WCParams);
		OverlayWidgetController->BindCallbacksToDependencies();
	}
	return OverlayWidgetController;
}

URPGAttributeMenuWidgetController* ARPG_MainHUD::GetAttributeMenuWidgetController(
	const FRPGWidgetControllerParams& WCParams)
{
	if(AttributeMenuWidgetController == nullptr)
	{
		AttributeMenuWidgetController = NewObject<URPGAttributeMenuWidgetController>(this, AttributeMenuWidgetControllerClass);
		AttributeMenuWidgetController->SetWidgetControllerParams(WCParams);
		AttributeMenuWidgetController->BindCallbacksToDependencies();
	}
	return AttributeMenuWidgetController;
}

URPGSpellMenuWidgetController* ARPG_MainHUD::GetSpellMenuWidgetController(const FRPGWidgetControllerParams& WCParams)
{
	if(SpellMenuWidgetController == nullptr)
	{
		SpellMenuWidgetController = NewObject<URPGSpellMenuWidgetController>(this, SpellMenuWidgetControllerClass);
		SpellMenuWidgetController->SetWidgetControllerParams(WCParams);
		SpellMenuWidgetController->BindCallbacksToDependencies();
	}
	return SpellMenuWidgetController;
}

void ARPG_MainHUD::InitOverlay(APlayerController* PC, APlayerState* PS, UAbilitySystemComponent* ASC, UAttributeSet* AS)
{
	if(PC->IsLocalController())
	{
		checkf(OverlayWidgetClass,TEXT("Overlay Widget Class is uninitialized, please fill out BP_RPGMainHUD"));
        checkf(OverlayWidgetControllerClass,TEXT("Overlay Widget Controller Class is uninitialized, please fill out BP_RPGMainHUD"));
        	
        UUserWidget* Widget = CreateWidget<UUserWidget>(GetWorld(),OverlayWidgetClass);
        RPGOverlayWidget = Cast<URPGUserWidget>(Widget);
        
        const FRPGWidgetControllerParams WidgetControllerParams(PC,PS,ASC,AS);
        URPGOverlayWidgetController* WidgetController = GetOverlayWidgetController(WidgetControllerParams);
        RPGOverlayWidget->SetWidgetController(WidgetController);
        WidgetController->BroadcastInitialValues();
        Widget->AddToViewport();
	}
	
	
}


