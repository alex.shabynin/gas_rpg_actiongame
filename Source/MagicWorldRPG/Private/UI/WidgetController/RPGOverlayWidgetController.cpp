// Copyright Alex Shabynin


#include "UI/WidgetController/RPGOverlayWidgetController.h"

#include "RPG_GameplayTags.h"
#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "AbilitySystem/RPGAttributeSet.h"
#include "AbilitySystem/Data/RPGAbilityInfo.h"
#include "AbilitySystem/Data/RPGLevelUpInfo.h"
#include "Core/RPG_PlayerState.h"

void URPGOverlayWidgetController::BroadcastInitialValues()
{
	const URPGAttributeSet* RPGAttributeSet = GetRPG_AS();

	OnHealthChanged.Broadcast(RPGAttributeSet->GetHealth());
	OnMaxHealthChanged.Broadcast(RPGAttributeSet->GetMaxHealth());
	OnManaChanged.Broadcast(RPGAttributeSet->GetMana());
	OnMaxManaChanged.Broadcast((RPGAttributeSet->GetMaxMana()));

	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(RPGAttributeSet->GetHealthAttribute());
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(RPGAttributeSet->GetManaAttribute());
}

void URPGOverlayWidgetController::BindCallbacksToDependencies()
{
	GetRPGPlayerState()->OnXPChangedDelegate.AddUObject(this, &URPGOverlayWidgetController::OnXPChanged);
	GetRPGPlayerState()->OnLevelChangedDelegate.AddLambda([this] (int32 NewLevel)
	{
		OnPlayerLevelChangedDelegate.Broadcast(NewLevel);
	});
	//AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(RPGAttributeSet->GetHealthAttribute()).AddUObject(this,&URPGOverlayWidgetController::HealthChanged);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(GetRPG_AS()->GetHealthAttribute()).AddLambda(
		[this] (const FOnAttributeChangeData& Data)
		{
			OnHealthChanged.Broadcast(Data.NewValue);
		}
		);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(GetRPG_AS()->GetMaxHealthAttribute()).AddLambda(
		[this] (const FOnAttributeChangeData& Data)
		{
			OnMaxHealthChanged.Broadcast(Data.NewValue);
		}
		);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(GetRPG_AS()->GetManaAttribute()).AddLambda(
		[this] (const FOnAttributeChangeData& Data)
		{
			OnManaChanged.Broadcast(Data.NewValue);
		}
		);
	AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(GetRPG_AS()->GetMaxManaAttribute()).AddLambda(
	[this] (const FOnAttributeChangeData& Data)
		{
			OnMaxManaChanged.Broadcast(Data.NewValue);
		}
		);
	
	if(GetRPG_ASC())
	{
		GetRPG_ASC()->AbilityEquippedDelegate.AddUObject(this,&URPGOverlayWidgetController::OnAbilityEquipped);
		
		if(GetRPG_ASC()->bStartupAbilitiesGiven)
		{
			//OnInitializeStartupAbilities(GetRPG_ASC());
			BroadcastAbilityInfo();
		}
		else
		{
			GetRPG_ASC()->AbilitiesGivenDelegate.AddUObject(this,&URPGOverlayWidgetController::BroadcastAbilityInfo);	
		}
		
		GetRPG_ASC()->EffectAssetTags.AddLambda(
        		[this] (const FGameplayTagContainer& AssetTags)
        		{
        			for(const FGameplayTag& Tag : AssetTags)
        			{
        				//"A.1".MatchesTag("A") will return True, "A".MatchesTag("A.1") will return False
        				FGameplayTag MessageTag = FGameplayTag::RequestGameplayTag(FName("Message"));
        				if(Tag.MatchesTag(MessageTag))
        				{
        					const FUIWidgetRow* Row =	GetDataTableRowByTag<FUIWidgetRow>(MessageWidgetDataTable,Tag);
                            MessageWidgetRowDelegate.Broadcast(*Row);
        				}
        			}
        		}
        		);
	}

}

void URPGOverlayWidgetController::OnXPChanged(int32 NewXP)
{
	const URPGLevelUpInfo* LevelUpInfo = GetRPGPlayerState()->LevelUpInfo;

	checkf(LevelUpInfo,TEXT("Unable to find LevelUpInfo DataAsset in PlayerState Blueprint"));

	const int32 Level = LevelUpInfo->FindLevelForXP(NewXP);
	const int32 MaxLevel = LevelUpInfo->LevelUpInformation.Num();

	if (Level < MaxLevel && Level >0)
	{
		const int32 LevelUpRequirements = LevelUpInfo->LevelUpInformation[Level].LevelUpRequirements;
		const int32 PreviousLevelUpRequirements = LevelUpInfo->LevelUpInformation[Level -1].LevelUpRequirements;
		const int32 DeltaLevelUpRequirements = LevelUpRequirements - PreviousLevelUpRequirements;
		const int32 XPForThisLevel = NewXP - PreviousLevelUpRequirements;

		const float XPBarPercent = static_cast<float>(XPForThisLevel) / static_cast<float>(DeltaLevelUpRequirements);

		OnXPPercentChangedDelegate.Broadcast(XPBarPercent);
	}
}

void URPGOverlayWidgetController::OnAbilityEquipped(const FGameplayTag& AbilityTag, const FGameplayTag& Status,
	const FGameplayTag& Slot, const FGameplayTag& PrevSlot) const
{

	const FRPG_GameplayTags& GameplayTags = FRPG_GameplayTags::Get();
	
	FRPG_AbilityInfo LastSlotInfo;
	LastSlotInfo.StatusTag = GameplayTags.Abilities_Status_Unlocked;
	LastSlotInfo.InputTag = PrevSlot;
	LastSlotInfo.AbilityTag = GameplayTags.Abilities_None;
	// Broadcast empty info if PreviousSlot is a valid slot. Only if equipping an already-equipped spell
	AbilityInfoDelegate.Broadcast(LastSlotInfo);

	FRPG_AbilityInfo Info = AbilityInfo->FindAbilityInfoByTag(AbilityTag);
	Info.StatusTag = Status;
	Info.InputTag = Slot;

	AbilityInfoDelegate.Broadcast(Info);

}


/*void URPGOverlayWidgetController::HealthChanged(const FOnAttributeChangeData& Data) const
{
	OnHealthChanged.Broadcast(Data.NewValue);

}*/
