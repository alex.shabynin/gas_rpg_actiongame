// Copyright Alex Shabynin


#include "UI/WidgetController/RPGAttributeMenuWidgetController.h"

#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "AbilitySystem/RPGAttributeInfo.h"
#include "AbilitySystem/RPGAttributeSet.h"
#include "Core/RPG_PlayerState.h"

void URPGAttributeMenuWidgetController::BindCallbacksToDependencies()
{
	check(AttributeInfo)
	for(auto& Pair : GetRPG_AS()->TagsToAttributes)
	{
		AbilitySystemComponent->GetGameplayAttributeValueChangeDelegate(Pair.Value()).AddLambda(
        	[this, Pair] (const FOnAttributeChangeData& Data)
        	{
				BroadcastAttributeInfo(Pair.Key,Pair.Value());
        	}
        	);
	}
	GetRPGPlayerState()->OnAttributePointsChangedDelegate.AddLambda([this](int32 InAttributePoints)
	{
		AttributePointsChangedDelegate.Broadcast(InAttributePoints);
	});
	
}

void URPGAttributeMenuWidgetController::BroadcastInitialValues()
{
	
	check(AttributeInfo)

	for (auto& Pair : GetRPG_AS()->TagsToAttributes)
	{
		BroadcastAttributeInfo(Pair.Key,Pair.Value());
	}
	
	AttributePointsChangedDelegate.Broadcast(GetRPGPlayerState()->GetAttributePoints());
	
}

void URPGAttributeMenuWidgetController::UpgradeAttribute(const FGameplayTag& AttributeTag)
{
	GetRPG_ASC()->UpgradeAttribute(AttributeTag);
}

void URPGAttributeMenuWidgetController::BroadcastAttributeInfo(const FGameplayTag& AttributeTag,
                                                               const FGameplayAttribute& Attribute) const
{
	FRPG_AttributeInfo Info = AttributeInfo->FindAttributeInfoForTag(AttributeTag);
	Info.AttributeValue = Attribute.GetNumericValue(AttributeSet);
	AttributeInfoDelegate.Broadcast(Info); 
}
