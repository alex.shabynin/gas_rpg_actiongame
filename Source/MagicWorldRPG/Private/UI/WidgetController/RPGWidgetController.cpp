// Copyright Alex Shabynin


#include "UI/WidgetController/RPGWidgetController.h"

#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "AbilitySystem/RPGAttributeSet.h"
#include "AbilitySystem/Data/RPGAbilityInfo.h"
#include "Core/RPGPlayerController.h"
#include "Core/RPG_PlayerState.h"

void URPGWidgetController::SetWidgetControllerParams(const FRPGWidgetControllerParams& WCParams)
{
	PlayerController = WCParams.PlayerController;
	PlayerState = WCParams.PlayerState;
	AbilitySystemComponent = WCParams.AbilitySystemComponent;
	AttributeSet = WCParams.AttributeSet;
}

void URPGWidgetController::BroadcastInitialValues()
{
	
}

void URPGWidgetController::BindCallbacksToDependencies()
{
	
}

void URPGWidgetController::BroadcastAbilityInfo()
{
	if(!GetRPG_ASC()->bStartupAbilitiesGiven)
	{
		return;
	}
	FForeachAbility BroadcastDelegate;
	BroadcastDelegate.BindLambda([this] (const FGameplayAbilitySpec& AbilitySpec)
	{
		FRPG_AbilityInfo Info = AbilityInfo->FindAbilityInfoByTag(RPGAbilitySystemComponent->GetAbilityTagFromSpec(AbilitySpec));
		Info.StatusTag = RPGAbilitySystemComponent->GetStatusFromSpec(AbilitySpec);
		Info.InputTag = RPGAbilitySystemComponent->GetInputTagFromSpec(AbilitySpec);
		AbilityInfoDelegate.Broadcast(Info);
	}
	);
	GetRPG_ASC()->ForEachAbility(BroadcastDelegate);
}

ARPGPlayerController* URPGWidgetController::GetRPGPlayerController()
{
	if(RPGPlayerController == nullptr)
	{
		RPGPlayerController = Cast<ARPGPlayerController>(PlayerController);
	}
	return RPGPlayerController;
}

ARPG_PlayerState* URPGWidgetController::GetRPGPlayerState()
{
	if(RPGPlayerState == nullptr)
	{
		RPGPlayerState = Cast<ARPG_PlayerState>(PlayerState);
	}
	return RPGPlayerState;
}

URPGAbilitySystemComponent* URPGWidgetController::GetRPG_ASC()
{
	if(RPGAbilitySystemComponent == nullptr)
	{
		RPGAbilitySystemComponent = Cast<URPGAbilitySystemComponent>(AbilitySystemComponent);
	}
	return RPGAbilitySystemComponent;
}

URPGAttributeSet* URPGWidgetController::GetRPG_AS()
{
	if(RPGAttribSet == nullptr)
	{
		RPGAttribSet = Cast<URPGAttributeSet>(AttributeSet);
	}
	return RPGAttribSet;
}
