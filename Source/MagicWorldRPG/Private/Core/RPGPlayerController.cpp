// Copyright Alex Shabynin


#include "Core/RPGPlayerController.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "EnhancedInputSubsystems.h"
#include "GameplayTagContainer.h"
#include "InputActionValue.h"
#include "NavigationPath.h"
#include "NavigationSystem.h"
#include "NiagaraFunctionLibrary.h"
#include "RPG_GameplayTags.h"
#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "Components/SplineComponent.h"
#include "GameFramework/Character.h"
#include "Input/RPG_InputComponent.h"
#include "Interaction/RPG_TargetInterface.h"
#include "UI/Widget/DamageTextWidgetComp.h"
#include "NiagaraSystem.h"
#include "Actors/RPGMagicCircle.h"
#include "Components/DecalComponent.h"
#include "MagicWorldRPG/MagicWorldRPG.h"

ARPGPlayerController::ARPGPlayerController()
{
	bReplicates = true;

	Spline = CreateDefaultSubobject<USplineComponent>("Spline");
}

void ARPGPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	CursorTrace();
	AutoRun();
	UpdateMagicCircleLocation();
}

void ARPGPlayerController::ShowDamageAmount_Implementation(float DamageAmount, ACharacter* TargetCharacter, bool bIsBlocked, bool bIsCritical)
{
	if(IsValid(TargetCharacter) && DamageTextComp && IsLocalController())
	{
		UDamageTextWidgetComp* DamageText = NewObject<UDamageTextWidgetComp>(TargetCharacter,DamageTextComp);
		DamageText->RegisterComponent();
		DamageText->AttachToComponent(TargetCharacter->GetRootComponent(),FAttachmentTransformRules::KeepRelativeTransform);
		DamageText->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
		DamageText->SetDamageText(DamageAmount, bIsBlocked, bIsCritical);
	}
}

void ARPGPlayerController::AutoRun()
{
	if(!bAutoRunning) return;
	if(APawn* ControlledPawn = GetPawn())
	{
		const FVector LocationOnSpline = Spline->FindLocationClosestToWorldLocation(ControlledPawn->GetActorLocation(),ESplineCoordinateSpace::World);
		const FVector Direction = Spline->FindDirectionClosestToWorldLocation(LocationOnSpline,ESplineCoordinateSpace::World);
		ControlledPawn->AddMovementInput(Direction);

		const float DistanceToDestination = (LocationOnSpline - CachedDestination).Length();
		if(DistanceToDestination <= AutoRunAcceptanceRadius)
		{
			bAutoRunning = false;
		}
		if(ControlledPawn->GetActorLocation() == CachedDestination)
		{
			bAutoRunning = false;
		}
	}
}

void ARPGPlayerController::UpdateMagicCircleLocation()
{
	if(IsValid(MagicCircle))
	{
		MagicCircle->SetActorLocation(CursorHit.ImpactPoint);
	}
}

void ARPGPlayerController::BeginPlay()
{
	Super::BeginPlay();

	AddInputMappingContext(PlayerMappingContext,0);

	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	FInputModeGameAndUI InputMode;
	InputMode.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);
	InputMode.SetHideCursorDuringCapture(false);
	SetInputMode(InputMode);
}

void ARPGPlayerController::AddInputMappingContext(UInputMappingContext* ContextToAdd, int32 InPriority)
{
	check(ContextToAdd)
	UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer());

	if(Subsystem)
	{
		Subsystem->AddMappingContext(ContextToAdd, InPriority);
	}
}

void ARPGPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	URPG_InputComponent* RPG_InputComponent = CastChecked<URPG_InputComponent>(InputComponent);
	check(RPG_InputComponent);

	RPG_InputComponent->BindAction(MoveAction,ETriggerEvent::Triggered,this, &ARPGPlayerController::Move);
	RPG_InputComponent->BindAction(ShiftAction,ETriggerEvent::Started,this, &ARPGPlayerController::ShiftPressed);
	RPG_InputComponent->BindAction(ShiftAction,ETriggerEvent::Completed,this, &ARPGPlayerController::ShiftReleased);
	RPG_InputComponent->BindAbilityActions(InputConfig,this, &ThisClass::AbilityInputTagPressed,&ThisClass::AbilityInputTagReleased, &ThisClass::AbilityInputTagHold);
}

void ARPGPlayerController::Move(const FInputActionValue& InputActionValue)
{
	APawn* ControlledPawn = GetPawn<APawn>();
	if(GetASC() && GetASC()->HasMatchingGameplayTag(FRPG_GameplayTags::Get().Player_Block_InputPressed))
	{
		return;
	}
	
	const FVector2D MovementVector = InputActionValue.Get<FVector2D>();
	const FRotator Rotation = GetControlRotation();
	const FRotator YawRotation(0.f,Rotation.Yaw,0.f);

	const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

	if (ControlledPawn)
	{
		ControlledPawn->AddMovementInput(ForwardDirection, MovementVector.Y);
        ControlledPawn->AddMovementInput(RightDirection, MovementVector.X);
	}
	
}

void ARPGPlayerController::ShiftPressed()
{
	bShiftKeyDown = true;
}

void ARPGPlayerController::ShiftReleased()
{
	bShiftKeyDown = false;
}

void ARPGPlayerController::ShowMagicCircle(UMaterialInterface* DecalMaterial)
{
	if(!IsValid(MagicCircle))
	{
		MagicCircle = GetWorld()->SpawnActor<ARPGMagicCircle>(MagicCircleClass);
		if(DecalMaterial)
		{
			MagicCircle->MagicCircleDecal->SetMaterial(0,DecalMaterial);
		}
	}
}

void ARPGPlayerController::HideMagicCircle()
{
	if(IsValid(MagicCircle))
	{
		MagicCircle->Destroy();
	}
}

void ARPGPlayerController::CursorTrace()
{
	APawn* ControlledPawn = GetPawn<APawn>();
	if(GetASC() && GetASC()->HasMatchingGameplayTag(FRPG_GameplayTags::Get().Player_Block_CursorTrace))
	{
		if(LastActor) LastActor->UnHighlightActor();
		if(ThisActor) ThisActor->UnHighlightActor();
		LastActor = nullptr;
		ThisActor = nullptr;
		return;
	}
	if(ControlledPawn && ControlledPawn->ActorHasTag(FName("Immune")))
	{
		return;
	}

	const ECollisionChannel TraceChannel = IsValid(MagicCircle) ? ECC_ExcludePlayers : ECC_Visibility;
	
	GetHitResultUnderCursor(TraceChannel,false,CursorHit);

	if(!CursorHit.bBlockingHit) return;

	LastActor = ThisActor;
	ThisActor = Cast<IRPG_TargetInterface>(CursorHit.GetActor());

	if( LastActor != ThisActor)
	{
		if(LastActor) LastActor->UnHighlightActor();
		if(ThisActor) ThisActor->HighlightActor();
	}
	/**
	 * Line trace from cursor. There are several scenarios:
	 *  A. LastActor is null && ThisActor is null
	 *		- Do nothing
	 *	B. LastActor is null && ThisActor is valid
	 *		- Highlight ThisActor
	 *	C. LastActor is valid && ThisActor is null
	 *		- UnHighlight LastActor
	 *	D. Both actors are valid, but LastActor != ThisActor
	 *		- UnHighlight LastActor, and Highlight ThisActor
	 *	E. Both actors are valid, and are the same actor
	 *		- Do nothing
	 */

	/*if (LastActor == nullptr)
	{
		if(ThisActor != nullptr)
		{
			ThisActor->HighlightActor();
		}
	}
	else
	{
		if (ThisActor == nullptr)
		{
			LastActor->UnHighlightActor();
		}
		else
		{
			if (LastActor !=ThisActor)
			{
				LastActor->UnHighlightActor();
                ThisActor->HighlightActor();
			}
		}
	}*/
	
}

void ARPGPlayerController::AbilityInputTagPressed(FGameplayTag InputTag)
{
	APawn* ControlledPawn = GetPawn<APawn>();
	if(GetASC() && GetASC()->HasMatchingGameplayTag(FRPG_GameplayTags::Get().Player_Block_InputPressed))
	{
		return;
	}
	if(ControlledPawn && ControlledPawn->ActorHasTag(FName("Immune")))
	{
		return;
	}
	if(InputTag.MatchesTagExact(FRPG_GameplayTags::Get().InputTag_LMB))
	{
		bTargeting = ThisActor ? true : false;
        bAutoRunning = false;
	}
	 if(GetASC()) GetASC()->AbilityInputTagPressed(InputTag);
}

void ARPGPlayerController::AbilityInputTagReleased(FGameplayTag InputTag)
{
	APawn* ControlledPawn = GetPawn<APawn>();
	if(GetASC() && GetASC()->HasMatchingGameplayTag(FRPG_GameplayTags::Get().Player_Block_InputReleased))
	{
		return;
	}
	if(ControlledPawn && ControlledPawn->ActorHasTag(FName("Immune")))
	{
		return;
	}
	if(!InputTag.MatchesTagExact(FRPG_GameplayTags::Get().InputTag_LMB))
	{
		if(GetASC())
		{
			GetASC()->AbilityInputTagReleased(InputTag);
		}
		return;
	}

	if(GetASC())
	{
		GetASC()->AbilityInputTagReleased(InputTag);
	}
	
	if(!bTargeting && !bShiftKeyDown)
	{
		if(FollowTime <= ShortPressThreshold && ControlledPawn)
		{
			if(UNavigationPath* NavPath = UNavigationSystemV1::FindPathToLocationSynchronously(
				this,ControlledPawn->GetActorLocation(),CachedDestination))
			{
				Spline->ClearSplinePoints();
				for (const FVector& PointLoc : NavPath->PathPoints)
				{
					Spline->AddSplinePoint(PointLoc,ESplineCoordinateSpace::World);
				}
				if(NavPath->PathPoints.Num() > 0)
				{
					CachedDestination = NavPath->PathPoints[NavPath->PathPoints.Num() -1];
                    bAutoRunning = true;
				}
			}
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(this,ClickNiagaraSystem,CachedDestination);
		}
		FollowTime = 0.f;
		bTargeting = false;
	}
}

void ARPGPlayerController::AbilityInputTagHold(FGameplayTag InputTag)
{
	APawn* ControlledPawn = GetPawn<APawn>();
	if(GetASC() && GetASC()->HasMatchingGameplayTag(FRPG_GameplayTags::Get().Player_Block_InputHeld))
	{
		return;
	}
	if(ControlledPawn && ControlledPawn->ActorHasTag(FName("Immune")))
	{
		return;
	}
	if(!InputTag.MatchesTagExact(FRPG_GameplayTags::Get().InputTag_LMB))
	{
		if(GetASC())
		{
			GetASC()->AbilityInputTagHold(InputTag);
		}
		return;
	}
	if(bTargeting || bShiftKeyDown)
	{
		if(GetASC())
		{
			GetASC()->AbilityInputTagHold(InputTag);
		}
	}
	else
	{
		FollowTime += GetWorld()->GetDeltaSeconds();

		if(CursorHit.bBlockingHit)
		{
			CachedDestination = CursorHit.ImpactPoint;
		}
		if(ControlledPawn)
		{
			const FVector WorldDirection = (CachedDestination - GetPawn()->GetActorLocation()).GetSafeNormal();
			ControlledPawn->AddMovementInput(WorldDirection);
		}
	}
}

URPGAbilitySystemComponent* ARPGPlayerController::GetASC()
{
	if(RPGAbilitySystemComponent == nullptr)
	{
		RPGAbilitySystemComponent = Cast<URPGAbilitySystemComponent>(UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetPawn<APawn>()));
	}
	return RPGAbilitySystemComponent;
}


