// Copyright Alex Shabynin


#include "AbilitySystem/Passive/RPGPassiveNiagaraComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystem/RPGAbilitySystemComponent.h"
#include "Interaction/RPG_CombatInterface.h"

URPGPassiveNiagaraComponent::URPGPassiveNiagaraComponent()
{
	bAutoActivate = false;
}

void URPGPassiveNiagaraComponent::BeginPlay()
{
	Super::BeginPlay();

	if(URPGAbilitySystemComponent* RPG_AbilitySC = Cast<URPGAbilitySystemComponent>(UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetOwner())))
	{
		RPG_AbilitySC->ActivatePassiveEffectDelegate.AddUObject(this,&URPGPassiveNiagaraComponent::OnPassiveAbilityActivated);
	}
	else if (IRPG_CombatInterface* CombatInterface = Cast<IRPG_CombatInterface>(GetOwner()))
	{
		CombatInterface->GetOnASCRegisteredDelegate().AddLambda([this] (UAbilitySystemComponent* ASC)
		{
			if(URPGAbilitySystemComponent* RPG_AbilitySC = Cast<URPGAbilitySystemComponent>(UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetOwner())))
			{
				RPG_AbilitySC->ActivatePassiveEffectDelegate.AddUObject(this,&URPGPassiveNiagaraComponent::OnPassiveAbilityActivated);
			}
		});
	}
	
}

void URPGPassiveNiagaraComponent::OnPassiveAbilityActivated(const FGameplayTag& AbilityTag, bool bActivate)
{
	if(AbilityTag.MatchesTagExact(PassiveSpellTag))
	{
		if(bActivate && !IsActive())
		{
			Activate();
		}
		else
		{
			Deactivate();
		}
	}
}
