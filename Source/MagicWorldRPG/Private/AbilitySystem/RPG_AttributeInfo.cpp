// Copyright Alex Shabynin


#include "AbilitySystem/RPGAttributeInfo.h"
#include "MagicWorldRPG/RPG_LogChannels.h"

FRPG_AttributeInfo URPGAttributeInfo::FindAttributeInfoForTag(const FGameplayTag& AttributeTag, bool bLogNotFound) const
{

	for(const FRPG_AttributeInfo& Info : AttributeInformation)
	{
		if(Info.AttributeTag.MatchesTagExact(AttributeTag))
		{
			return Info;
		}
	}
	if(bLogNotFound)
	{
		UE_LOG(LogRPG,Error,TEXT("Can't find info for AttributeTag [%s] on AttributeInfo [%s]."),*AttributeTag.ToString(),*GetNameSafe(this));
	}

	return FRPG_AttributeInfo();
}
