// Copyright Alex Shabynin


#include "AbilitySystem/RPGAbilitySystemGlobals.h"

#include "RPGAbilitySystemTypes.h"

FGameplayEffectContext* URPGAbilitySystemGlobals::AllocGameplayEffectContext() const
{
	
	return new FRPG_GameplayEffectContext();
}
