// Copyright Alex Shabynin


#include "AbilitySystem/ExecCalc/ExecCalc_Damage.h"

#include "AbilitySystemComponent.h"
#include "RPGAbilitySystemTypes.h"
#include "RPG_GameplayTags.h"
#include "AbilitySystem/RPGAbilitySystemLibrary.h"
#include "AbilitySystem/RPGAttributeSet.h"
#include "AbilitySystem/Data/RPGCharacterClassInfo.h"
#include "Interaction/RPG_CombatInterface.h"
#include "Kismet/GameplayStatics.h"

struct RPGDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(Armor);
	DECLARE_ATTRIBUTE_CAPTUREDEF(ArmorPenetration);
	DECLARE_ATTRIBUTE_CAPTUREDEF(BlockChance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(CritHitChance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(CritHitDamage);
	DECLARE_ATTRIBUTE_CAPTUREDEF(CritHitResistance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(FireResistance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(LightningResistance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(ArcaneResistance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(PhysicalResistance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(IceResistance);
	DECLARE_ATTRIBUTE_CAPTUREDEF(EarthResistance);

	//TMap<FGameplayTag, FGameplayEffectAttributeCaptureDefinition> TagsToCaptureDefs;
	
	RPGDamageStatics()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,Armor,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,BlockChance,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,ArmorPenetration,Source,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,CritHitChance,Source,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,CritHitDamage,Source,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,CritHitResistance,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,FireResistance,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,LightningResistance,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,ArcaneResistance,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,PhysicalResistance,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,IceResistance,Target,false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet,EarthResistance,Target,false);
		
	}
};

static const RPGDamageStatics& DamageStatics()
{
	static RPGDamageStatics DStatic;

	return DStatic;
}

UExecCalc_Damage::UExecCalc_Damage()
{
	RelevantAttributesToCapture.Add(DamageStatics().ArmorDef);
	RelevantAttributesToCapture.Add(DamageStatics().BlockChanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().ArmorPenetrationDef);
	RelevantAttributesToCapture.Add(DamageStatics().CritHitChanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().CritHitDamageDef);
	RelevantAttributesToCapture.Add(DamageStatics().CritHitResistanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().FireResistanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().LightningResistanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().ArcaneResistanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().PhysicalResistanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().IceResistanceDef);
	RelevantAttributesToCapture.Add(DamageStatics().EarthResistanceDef);

}

void UExecCalc_Damage::DetermineDebuff(const FGameplayEffectCustomExecutionParameters& ExecutionParams, const FGameplayEffectSpec& Spec, FAggregatorEvaluateParameters EvaluationParameters,const TMap<FGameplayTag,
	FGameplayEffectAttributeCaptureDefinition>& InTagsToDefs) const
{
	const FRPG_GameplayTags& GameplayTags = FRPG_GameplayTags::Get();

	for(TTuple<FGameplayTag,FGameplayTag> Pair : GameplayTags.DamageTypesToDebuff)
	{
		const FGameplayTag& DamageType = Pair.Key;
		const FGameplayTag& DebuffType = Pair.Value;
		const float TypeDamage = Spec.GetSetByCallerMagnitude(DamageType,false,-1.f);
		if(TypeDamage > -0.5f)
		{
			// Determine if there was a successful debuff
			const float SourceDebuffChance = Spec.GetSetByCallerMagnitude(GameplayTags.Debuff_Chance, false, -1.f);
			
			float TargetDebuffResistance = 0.f;
			const FGameplayTag& ResistanceTag = GameplayTags.DamageTypesToResistances[DamageType];
			ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(InTagsToDefs[ResistanceTag],EvaluationParameters,TargetDebuffResistance);
			TargetDebuffResistance = FMath::Max<float>(TargetDebuffResistance,0.f);
			const float EffectiveDebuffChance = SourceDebuffChance * (100 - TargetDebuffResistance) / 100.f;
			const bool bDebuff = FMath::RandRange(1,100) < EffectiveDebuffChance;
			if(bDebuff)
			{
				FGameplayEffectContextHandle ContextHandle = Spec.GetContext();

				URPGAbilitySystemLibrary::SetIsSuccessfulDebuff(ContextHandle,bDebuff);
				URPGAbilitySystemLibrary::SetDamageType(ContextHandle,DamageType);
				
				const float DebuffDamage = Spec.GetSetByCallerMagnitude(GameplayTags.Debuff_Damage,false,-1.f);
				const float DebuffDuration = Spec.GetSetByCallerMagnitude(GameplayTags.Debuff_Duration,false,-1.f);
				const float DebuffFrequency = Spec.GetSetByCallerMagnitude(GameplayTags.Debuff_Frequency,false,-1.f);
				
				URPGAbilitySystemLibrary::SetDebuffDamage(ContextHandle,DebuffDamage);
				URPGAbilitySystemLibrary::SetDebuffDuration(ContextHandle,DebuffDuration);
				URPGAbilitySystemLibrary::SetDebuffFrequency(ContextHandle,DebuffFrequency);
				//TODO: Debuff logic
			}
		}
	}
}

void UExecCalc_Damage::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& ExecutionParams,
                                              FGameplayEffectCustomExecutionOutput& OutExecutionOutput) const
{
	TMap<FGameplayTag,FGameplayEffectAttributeCaptureDefinition> TagsToCaptureDefs;
	
	const FRPG_GameplayTags& Tags = FRPG_GameplayTags::Get();
		
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_Armor,DamageStatics().ArmorDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_BlockChance,DamageStatics().BlockChanceDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_ArmorPenetration,DamageStatics().ArmorPenetrationDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_CritHitChance,DamageStatics().CritHitChanceDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_CritHitDamage,DamageStatics().CritHitDamageDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Secondary_CritHitResistance,DamageStatics().CritHitResistanceDef);
		
	TagsToCaptureDefs.Add(Tags.Attributes_Resistance_Fire,DamageStatics().FireResistanceDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Resistance_Lightning,DamageStatics().LightningResistanceDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Resistance_Arcane,DamageStatics().ArcaneResistanceDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Resistance_Physical,DamageStatics().PhysicalResistanceDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Resistance_Ice,DamageStatics().IceResistanceDef);
	TagsToCaptureDefs.Add(Tags.Attributes_Resistance_Earth,DamageStatics().EarthResistanceDef);
	
	const UAbilitySystemComponent* SourceASC = ExecutionParams.GetSourceAbilitySystemComponent();
	const UAbilitySystemComponent* TargetASC = ExecutionParams.GetTargetAbilitySystemComponent();

	AActor* SourceAvatarActor = SourceASC ? SourceASC->GetAvatarActor() : nullptr;
	AActor* TargetAvatarActor = TargetASC ? TargetASC->GetAvatarActor() : nullptr;

	int32 SourcePlayerLevel = 1;
	int32 TargetPlayerLevel = 1;
	if(SourceAvatarActor->Implements<URPG_CombatInterface>())
	{
		SourcePlayerLevel = IRPG_CombatInterface::Execute_GetPlayerLevel(SourceAvatarActor);
	}
	if(TargetAvatarActor->Implements<URPG_CombatInterface>())
	{
		TargetPlayerLevel = IRPG_CombatInterface::Execute_GetPlayerLevel(TargetAvatarActor);
	}
		
	const FGameplayEffectSpec& Spec = ExecutionParams.GetOwningSpec();
	FGameplayEffectContextHandle EffectContextHandle = Spec.GetContext();
	
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();
	
	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	// Debuff

	DetermineDebuff(ExecutionParams, Spec, EvaluationParameters,TagsToCaptureDefs);

	//Get Damage set by Caller magnitude
	//float Damage = Spec.GetSetByCallerMagnitude(FRPG_GameplayTags::Get().Damage); - old realization
	float Damage = 0.f;

	for(const TTuple<FGameplayTag, FGameplayTag>& Pair : FRPG_GameplayTags::Get().DamageTypesToResistances)
	{
		const FGameplayTag DamageTypeTag = Pair.Key;
		const FGameplayTag ResistanceTag = Pair.Value;

		checkf(TagsToCaptureDefs.Contains(ResistanceTag),TEXT("TagsToCaptureDefs doesn't contain Tag: [%s] in ExecCalc_Damage"), *ResistanceTag.ToString());
		const FGameplayEffectAttributeCaptureDefinition CaptureDef = TagsToCaptureDefs[ResistanceTag];
		
		float DamageTypeValue = Spec.GetSetByCallerMagnitude(Pair.Key,false);
		if(DamageTypeValue <= 0.f)
		{
			continue;
		}
		float Resistance = 0.f;
		ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(CaptureDef,EvaluationParameters,Resistance);
		Resistance = FMath::Clamp(Resistance,0.f,100.f);
		
		DamageTypeValue *= ( 100.f - Resistance) /100.f;

		if(URPGAbilitySystemLibrary::IsRadialDamage(EffectContextHandle))
		{
			// 1. override TakeDamage in RPGCharacterBase. *
			// 2. create delegate OnDamageDelegate, broadcast damage received in TakeDamage *
			// 3. Bind lambda to OnDamageDelegate on the Victim here. *
			// 4. Call UGameplayStatics::ApplyRadialDamageWithFalloff to cause damage (this will result in TakeDamage being called
			//		on the Victim, which will then broadcast OnDamageDelegate)
			// 5. In Lambda, set DamageTypeValue to the damage received from the broadcast *

			if(IRPG_CombatInterface* CombatInterface = Cast<IRPG_CombatInterface>(TargetAvatarActor))
			{
				CombatInterface->GetOnDamageSignature().AddLambda([&] (float DamageAmount)
				{
					DamageTypeValue = DamageAmount;
				});
			}
			UGameplayStatics::ApplyRadialDamageWithFalloff(TargetAvatarActor,
				DamageTypeValue,
				0.f,
				URPGAbilitySystemLibrary::GetRadialDamageOrigin(EffectContextHandle),
				URPGAbilitySystemLibrary::GetRadialDamageInnerRadius(EffectContextHandle),
				URPGAbilitySystemLibrary::GetRadialDamageOuterRadius(EffectContextHandle),
				1.f,
				UDamageType::StaticClass(),
				TArray<AActor*>(),
				SourceAvatarActor,nullptr);
		}
		
		Damage += DamageTypeValue;
	}

	//Capture BlockChance on Target and determine if there was a successful Block
	float TargetBlockChance = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().BlockChanceDef,EvaluationParameters,TargetBlockChance);
	Damage = FMath::Max<float>(0.f,Damage);

	const bool bBlocked = FMath::RandRange(1,100) < TargetBlockChance;

	URPGAbilitySystemLibrary::SetIsBlockedHit(EffectContextHandle,bBlocked);
	//RPGEffectContext->SetIsBlockedHit(bBlocked);
	
	//If Block, halve the Damage
	Damage = bBlocked ? Damage /2.f : Damage;
	
	float TargetArmor = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().ArmorDef,EvaluationParameters,TargetArmor);
	TargetArmor = FMath::Max<float>(0.f,TargetArmor);
	
	float SourceArmorPenetration = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().ArmorPenetrationDef,EvaluationParameters,SourceArmorPenetration);
	SourceArmorPenetration = FMath::Max<float>(0.f,SourceArmorPenetration);

	//Reading CurveTable data from DamageCalcCurveTable
	const URPGCharacterClassInfo* CharacterClassInfo = URPGAbilitySystemLibrary::GetCharacterClassInfo(SourceAvatarActor);
	const FRealCurve* ArmorPenetrationCurve = CharacterClassInfo->DamageCalcCurveTable->FindCurve(FName("ArmorPenetration"),FString());
	const FRealCurve* EffectiveArmorCurve = CharacterClassInfo->DamageCalcCurveTable->FindCurve(FName("EffectiveArmor"),FString());

	//Set Calculation coefficient, based on actor's current level
	const float ArmorPenetrationCoefficient = ArmorPenetrationCurve->Eval(SourcePlayerLevel);
	const float EffectiveArmorCoefficient = EffectiveArmorCurve->Eval(TargetPlayerLevel);

	
	//Armor penetration ignores percentage of the Target's Armor
	const float EffectiveArmor = TargetArmor *= (100 - SourceArmorPenetration * ArmorPenetrationCoefficient) /100.f;
	
	//Armor ignores percentage of the Source's Damage
	Damage *= (100 - EffectiveArmor * EffectiveArmorCoefficient) / 100.f;

	//Chance to execute Critical Hit
	float SourceCritHitChance = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().CritHitChanceDef, EvaluationParameters,SourceCritHitChance);
	SourceCritHitChance = FMath::Max<float>(0.f,SourceCritHitChance);

	float SourceCritHitDamage = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().CritHitDamageDef,EvaluationParameters,SourceCritHitDamage);
	SourceCritHitDamage = FMath::Max<float>(0.f,SourceCritHitDamage);

	float TargetCritHitResistance = 0.f;
	ExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().CritHitResistanceDef,EvaluationParameters,TargetCritHitResistance);
	TargetCritHitResistance = FMath::Max<float>(0.f,TargetCritHitResistance);

	const FRealCurve* CritHitResistanceCurve = CharacterClassInfo->DamageCalcCurveTable->FindCurve(FName("CritHitResistance"),FString());
	const float CritHitResistanceCoefficient = CritHitResistanceCurve->Eval(TargetPlayerLevel);

	//Critical Hit resistance reduces Critical Hit chance by certain percent
	const float EffectiveCritHitChance = SourceCritHitChance - TargetCritHitResistance * CritHitResistanceCoefficient;
	bool bIsCritical = FMath::RandRange(1,100) < EffectiveCritHitChance;

	URPGAbilitySystemLibrary::SetIsCriticalHit(EffectContextHandle,bIsCritical);
	//RPGEffectContext->SetIsCriticalHit(bIsCritical);

	//If critical hit - doing double amount of damage + bonus (SourceCritHitDamage)
	Damage = bIsCritical ? Damage *2.f + SourceCritHitDamage : Damage;
	
	const FGameplayModifierEvaluatedData EvaluatedData(URPGAttributeSet::GetIncomingDamageAttribute(),EGameplayModOp::Additive,Damage);
	OutExecutionOutput.AddOutputModifier(EvaluatedData);
}
