// Copyright Alex Shabynin


#include "AbilitySystem/Debuff/RPGDebuffNiagaraComponent.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "Interaction/RPG_CombatInterface.h"

URPGDebuffNiagaraComponent::URPGDebuffNiagaraComponent()
{
	bAutoActivate = false;
}

void URPGDebuffNiagaraComponent::BeginPlay()
{
	Super::BeginPlay();
	
	IRPG_CombatInterface* CombatInterface = Cast<IRPG_CombatInterface>(GetOwner());
	if(UAbilitySystemComponent* ASC = UAbilitySystemBlueprintLibrary::GetAbilitySystemComponent(GetOwner()))
	{
		ASC->RegisterGameplayTagEvent(DebuffTag,EGameplayTagEventType::NewOrRemoved).AddUObject(this,&URPGDebuffNiagaraComponent::DebuffTagChanged);
	}
	else if(CombatInterface)
	{
		CombatInterface->GetOnASCRegisteredDelegate().AddWeakLambda(this,[this](UAbilitySystemComponent* InASC)
		{
			InASC->RegisterGameplayTagEvent(DebuffTag,EGameplayTagEventType::NewOrRemoved).AddUObject(this,&URPGDebuffNiagaraComponent::DebuffTagChanged);
		});
	}
	if(CombatInterface)
	{
		CombatInterface->GetOnDeathDelegate().AddDynamic(this,&URPGDebuffNiagaraComponent::OnOwnerDeath);
	}
}

void URPGDebuffNiagaraComponent::DebuffTagChanged(const FGameplayTag CallbackTag, int32 NewCount)
{
	const bool bOwnerValid = IsValid(GetOwner());
	const bool bOwnerAlive = GetOwner()->Implements<URPG_CombatInterface>() && !IRPG_CombatInterface::Execute_IsDead(GetOwner());
	
	if(NewCount > 0 && bOwnerValid && bOwnerAlive)
	{
		Activate();
	}
	else
	{
		Deactivate();
	}
}

void URPGDebuffNiagaraComponent::OnOwnerDeath(AActor* DeadActor)
{
	Deactivate();
}
