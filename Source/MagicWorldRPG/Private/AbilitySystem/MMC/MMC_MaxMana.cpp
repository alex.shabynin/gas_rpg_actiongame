// Copyright Alex Shabynin


#include "AbilitySystem/MMC/MMC_MaxMana.h"

#include "AbilitySystem/RPGAttributeSet.h"
#include "Interaction/RPG_CombatInterface.h"

UMMC_MaxMana::UMMC_MaxMana()
{
	IntelligenceDef.AttributeToCapture = URPGAttributeSet::GetIntelligenceAttribute();
	IntelligenceDef.AttributeSource = EGameplayEffectAttributeCaptureSource::Target;
	IntelligenceDef.bSnapshot = false;

	RelevantAttributesToCapture.Add(IntelligenceDef);
}

float UMMC_MaxMana::CalculateBaseMagnitude_Implementation(const FGameplayEffectSpec& Spec) const
{
	//Gather tags from source and target
	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParameters;
	EvaluationParameters.SourceTags = SourceTags;
	EvaluationParameters.TargetTags = TargetTags;

	float Intelligence = 0.f;

	GetCapturedAttributeMagnitude(IntelligenceDef,Spec,EvaluationParameters,Intelligence);
	Intelligence = FMath::Max<float>(Intelligence,0.f);

	int32 PlayerLevel = 1;
	if(Spec.GetContext().GetSourceObject()->Implements<URPG_CombatInterface>())
	{
		PlayerLevel = IRPG_CombatInterface::Execute_GetPlayerLevel(Spec.GetContext().GetSourceObject());
	}

	return PostMAV + Coefficient * Intelligence + PreMAV * PlayerLevel;
}
