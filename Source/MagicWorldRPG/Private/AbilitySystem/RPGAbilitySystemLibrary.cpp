// Copyright Alex Shabynin


#include "AbilitySystem/RPGAbilitySystemLibrary.h"

#include "AbilitySystemBlueprintLibrary.h"
#include "AbilitySystemComponent.h"
#include "RPGAbilitySystemTypes.h"
#include "RPG_GameplayTags.h"
#include "AbilitySystem/Abilities/RPGGameplayAbility.h"
#include "Core/RPGGameModeBase.h"
#include "Core/RPG_PlayerState.h"
#include "Interaction/RPG_CombatInterface.h"
#include "Kismet/GameplayStatics.h"
#include "UI/HUD/RPG_MainHUD.h"
#include "UI/WidgetController/RPGWidgetController.h"


bool URPGAbilitySystemLibrary::MakeWidgetControllerParams(const UObject* WorldContextObject, FRPGWidgetControllerParams& OutWCParams,ARPG_MainHUD*& OutMainHUD)
{
	if(APlayerController* PC = UGameplayStatics::GetPlayerController(WorldContextObject,0))
	{
		OutMainHUD = Cast<ARPG_MainHUD>(PC->GetHUD());
		if(OutMainHUD)
		{
			ARPG_PlayerState* PS = PC->GetPlayerState<ARPG_PlayerState>();
			UAbilitySystemComponent* ASC = PS->GetAbilitySystemComponent();
			UAttributeSet* AS = PS->GetAttributeSet();

			OutWCParams.AttributeSet = AS;
			OutWCParams.PlayerController = PC;
			OutWCParams.PlayerState = PS;
			OutWCParams.AbilitySystemComponent = ASC;
			return true;
		}
	}
	return false;
}

URPGOverlayWidgetController* URPGAbilitySystemLibrary::GetOverlayWidgetController(
	const UObject* WorldContextObject)
{
	FRPGWidgetControllerParams WCParams;
	ARPG_MainHUD* MainHUD = nullptr;

	if(MakeWidgetControllerParams(WorldContextObject,WCParams,MainHUD))
	{
		return MainHUD->GetOverlayWidgetController(WCParams);
	}
	return nullptr;
}

URPGAttributeMenuWidgetController* URPGAbilitySystemLibrary::GetAttributeMenuWidgetController(
	const UObject* WorldContextObject)
{
	FRPGWidgetControllerParams WCParams;
	ARPG_MainHUD* MainHUD = nullptr;

	if(MakeWidgetControllerParams(WorldContextObject,WCParams,MainHUD))
	{
		return MainHUD->GetAttributeMenuWidgetController(WCParams);
	}
	return nullptr;
}

URPGSpellMenuWidgetController* URPGAbilitySystemLibrary::GetSpellMenuWidgetController(
	const UObject* WorldContextObject)
{
	FRPGWidgetControllerParams WCParams;
	ARPG_MainHUD* MainHUD = nullptr;

	if(MakeWidgetControllerParams(WorldContextObject,WCParams,MainHUD))
	{
		return MainHUD->GetSpellMenuWidgetController(WCParams);
	}
	return nullptr;
}

void URPGAbilitySystemLibrary::InitializeDefaultAttributes(const UObject* WorldContextObject, ECharacterClass CharacterClass, float Level,UAbilitySystemComponent* ASC)
{
	AActor* AvatarActor = ASC->GetAvatarActor();
	
	URPGCharacterClassInfo* CharacterClassInfo = GetCharacterClassInfo(WorldContextObject);
	FCharacterClassDefaultInfo ClassDefaultInfo = CharacterClassInfo->GetClassDefaultInfo(CharacterClass);

	FGameplayEffectContextHandle PrimaryAttributesContextHandle = ASC->MakeEffectContext();
	PrimaryAttributesContextHandle.AddSourceObject(AvatarActor);

	const FGameplayEffectSpecHandle PrimaryAttributesSpecHandle = ASC->MakeOutgoingSpec(ClassDefaultInfo.PrimaryAttributes,Level,PrimaryAttributesContextHandle);
	ASC->ApplyGameplayEffectSpecToSelf(*PrimaryAttributesSpecHandle.Data.Get());

	FGameplayEffectContextHandle SecondaryAttributesContextHandle = ASC->MakeEffectContext();
	SecondaryAttributesContextHandle.AddSourceObject(AvatarActor);
	
	const FGameplayEffectSpecHandle SecondaryAttributesSpecHandle = ASC->MakeOutgoingSpec(CharacterClassInfo->SecondaryAttributes,Level,SecondaryAttributesContextHandle);
	ASC->ApplyGameplayEffectSpecToSelf(*SecondaryAttributesSpecHandle.Data.Get());

	FGameplayEffectContextHandle VitalAttributesContextHandle = ASC->MakeEffectContext();
	VitalAttributesContextHandle.AddSourceObject(AvatarActor);
	
	const FGameplayEffectSpecHandle VitalAttributesSpecHandle = ASC->MakeOutgoingSpec(CharacterClassInfo->VitalAttributes,Level,VitalAttributesContextHandle);
	ASC->ApplyGameplayEffectSpecToSelf(*VitalAttributesSpecHandle.Data.Get());
}

void URPGAbilitySystemLibrary::GiveStartupAbilities(const UObject* WorldContextObject, UAbilitySystemComponent* ASC, ECharacterClass CharacterClass)
{
	URPGCharacterClassInfo* ClassInfo = GetCharacterClassInfo(WorldContextObject);

	if (ClassInfo == nullptr) return;
	for (TSubclassOf<UGameplayAbility> AbilityClass : ClassInfo->Abilities)
	{
		FGameplayAbilitySpec AbilitySpec = FGameplayAbilitySpec(AbilityClass,1);
		
		ASC->GiveAbility(AbilitySpec);
	}
	const FCharacterClassDefaultInfo& DefaultInfo = ClassInfo->GetClassDefaultInfo(CharacterClass);
	for (TSubclassOf<UGameplayAbility> AbilityClass : DefaultInfo.StartupAbilities)
	{
		if(ASC->GetAvatarActor()->Implements<URPG_CombatInterface>())
		{
			FGameplayAbilitySpec AbilitySpec = FGameplayAbilitySpec(AbilityClass,IRPG_CombatInterface::Execute_GetPlayerLevel(ASC->GetAvatarActor()));
            ASC->GiveAbility(AbilitySpec);
		}
	}
}

URPGCharacterClassInfo* URPGAbilitySystemLibrary::GetCharacterClassInfo(const UObject* WorldContextObject)
{
	const ARPGGameModeBase* RPGGameMode = Cast<ARPGGameModeBase>(UGameplayStatics::GetGameMode(WorldContextObject));
	if(RPGGameMode == nullptr) return nullptr;

	return RPGGameMode->CharacterClassInfo;
	
}

bool URPGAbilitySystemLibrary::IsBlockedHit(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->IsBlockedHit();
	}
	return false;
}

bool URPGAbilitySystemLibrary::IsCriticalHit(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->IsCriticalHit();
	}
	return false;
}

bool URPGAbilitySystemLibrary::IsSuccessfulDebuff(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->IsSuccessfulDebuff();
	}
	return false;
}

float URPGAbilitySystemLibrary::GetDebuffDamage(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetDebuffDamage();
	}
	return 0.f;
}

float URPGAbilitySystemLibrary::GetDebuffDuration(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetDebuffDuration();
	}
	return 0.f;
}

float URPGAbilitySystemLibrary::GetDebuffFrequency(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetDebuffFrequency();
	}
	return 0.f;
}

FGameplayTag URPGAbilitySystemLibrary::GetDamageType(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		if(RPGEffectContext->GetDamageType().IsValid())
		{
			return *RPGEffectContext->GetDamageType();
		}
	}
	return FGameplayTag();
}

FVector URPGAbilitySystemLibrary::GetDeathImpulse(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetDeathImpulse();
	}
	return FVector::ZeroVector;
}

FVector URPGAbilitySystemLibrary::GetKnockBackForce(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetKnockBackForce();
	}
	return FVector::ZeroVector;
}

bool URPGAbilitySystemLibrary::IsRadialDamage(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->IsRadialDamage();
	}
	return false; 
}

float URPGAbilitySystemLibrary::GetRadialDamageInnerRadius(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetRadialDamageInnerRadius();
	}
	return 0.f;
}

float URPGAbilitySystemLibrary::GetRadialDamageOuterRadius(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetRadialDamageOuterRadius();
	}
	return 0.f;
}

FVector URPGAbilitySystemLibrary::GetRadialDamageOrigin(const FGameplayEffectContextHandle& EffectContextHandle)
{
	if(const FRPG_GameplayEffectContext* RPGEffectContext = static_cast<const FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		return RPGEffectContext->GetRadialDamageOrigin();
	}
	return FVector::ZeroVector;
}

void URPGAbilitySystemLibrary::SetIsBlockedHit(FGameplayEffectContextHandle& EffectContextHandle, bool bIsInBlockedHit)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetIsBlockedHit(bIsInBlockedHit);
	}
	
}

void URPGAbilitySystemLibrary::SetIsCriticalHit(FGameplayEffectContextHandle& EffectContextHandle, bool bIsInCriticalHit)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetIsCriticalHit(bIsInCriticalHit);
	}
	
}

void URPGAbilitySystemLibrary::SetIsSuccessfulDebuff(FGameplayEffectContextHandle& EffectContextHandle,
	bool bIsInSuccessfullDebuff)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetIsSuccessfulDebuff(bIsInSuccessfullDebuff);
	}
}

void URPGAbilitySystemLibrary::SetDebuffDamage(FGameplayEffectContextHandle& EffectContextHandle, float InDebuffDamage)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetDebuffDamage(InDebuffDamage);
	}
}

void URPGAbilitySystemLibrary::SetDebuffDuration(FGameplayEffectContextHandle& EffectContextHandle,
	float InDebuffDuration)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetDebuffDuration(InDebuffDuration);
	}
}

void URPGAbilitySystemLibrary::SetDebuffFrequency(FGameplayEffectContextHandle& EffectContextHandle,
	float InDebuffFrequency)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetDebuffFrequency(InDebuffFrequency);
	}
}

void URPGAbilitySystemLibrary::SetDamageType(FGameplayEffectContextHandle& EffectContextHandle,
	const FGameplayTag& InDamageType)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		const TSharedPtr<FGameplayTag> DamageType = MakeShared<FGameplayTag>(InDamageType);
		RPGEffectContext->SetDamageType(DamageType);
	}
}

void URPGAbilitySystemLibrary::SetDeathImpulse(FGameplayEffectContextHandle& EffectContextHandle,
	const FVector& InDeathImpulse)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetDeathImpulse(InDeathImpulse);
	}
}

void URPGAbilitySystemLibrary::SetKnockBackForce(FGameplayEffectContextHandle& EffectContextHandle,
	const FVector& InKnockBackForce)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetKnockBackForce(InKnockBackForce);
	}
}

void URPGAbilitySystemLibrary::SetIsRadialDamage(FGameplayEffectContextHandle& EffectContextHandle,
	bool bInIsRadialDamage)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetIsRadialDamage(bInIsRadialDamage);
	}

}

void URPGAbilitySystemLibrary::SetRadialInnerRadius(FGameplayEffectContextHandle& EffectContextHandle,
	float InRadialInnerRadius)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetRadialDamageInnerRadius(InRadialInnerRadius);
	}
}

void URPGAbilitySystemLibrary::SetRadialOuterRadius(FGameplayEffectContextHandle& EffectContextHandle,
	float InRadialOuterRadius)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetRadialDamageOuterRadius(InRadialOuterRadius);
	}
}

void URPGAbilitySystemLibrary::SetRadialOrigin(FGameplayEffectContextHandle& EffectContextHandle,
	const FVector& InRadialOrigin)
{
	if(FRPG_GameplayEffectContext* RPGEffectContext = static_cast<FRPG_GameplayEffectContext*>(EffectContextHandle.Get()))
	{
		RPGEffectContext->SetRadialDamageOrigin(InRadialOrigin);
	}
}

void URPGAbilitySystemLibrary::GetLivePlayersWithinRadius(const UObject* WorldContextObject,
                                                          TArray<AActor*>& OutOverlappingActors,const TArray<AActor*>& ActorsToIgnore, float Radius, const FVector& SphereOrigin)
{
	FCollisionQueryParams SphereParams;
	SphereParams.AddIgnoredActors(ActorsToIgnore);

	TArray<FOverlapResult> Overlaps;
	if (const UWorld* World = GEngine->GetWorldFromContextObject(WorldContextObject, EGetWorldErrorMode::LogAndReturnNull))
	{
		World->OverlapMultiByObjectType(Overlaps, SphereOrigin, FQuat::Identity, FCollisionObjectQueryParams(FCollisionObjectQueryParams::InitType::AllDynamicObjects), FCollisionShape::MakeSphere(Radius), SphereParams);
		for(FOverlapResult& Overlap : Overlaps)
		{
			if(Overlap.GetActor()->Implements<URPG_CombatInterface>() && !IRPG_CombatInterface::Execute_IsDead(Overlap.GetActor()))
			{
				OutOverlappingActors.AddUnique(IRPG_CombatInterface::Execute_GetAvatar(Overlap.GetActor()));
			}
		}
	}
}

void URPGAbilitySystemLibrary::GetClosestTargets(int32 MaxTargets, const TArray<AActor*>& Actors,
	TArray<AActor*>& OutClosestTargets, const FVector& Origin)
{
	if(Actors.Num() <= MaxTargets)
	{
		OutClosestTargets = Actors;
		return;
	}
	TArray<AActor*> ActorsToCheck = Actors;
	int32 NumTargetsFound = 0;

	while(NumTargetsFound < MaxTargets)
	{
		if(ActorsToCheck.Num() == 0) break;

		double ClosestDistance = TNumericLimits<double>::Max();
		AActor* ClosestActor;

		for(AActor* PotentialTarget : ActorsToCheck)
		{
			const double Distance = (PotentialTarget->GetActorLocation() - Origin).Length();

			if(Distance < ClosestDistance)
			{
				ClosestDistance = Distance;
				ClosestActor = PotentialTarget;
			}
		}
		ActorsToCheck.Remove(ClosestActor);	
        OutClosestTargets.AddUnique(ClosestActor);
        ++NumTargetsFound;
	}
	
}

bool URPGAbilitySystemLibrary::IsNotFriendlyActor(AActor* FirstActor, AActor* SecondActor)
{
	const bool bBothArePlayers = FirstActor->ActorHasTag(FName("Player")) && SecondActor->ActorHasTag(FName("Player"));
	const bool bBothAreEnemies = FirstActor->ActorHasTag(FName("Enemy")) && SecondActor->ActorHasTag(FName("Enemy"));

	const bool bFriends = bBothArePlayers || bBothAreEnemies;

	return !bFriends;
}

int32 URPGAbilitySystemLibrary::GetXPRewardForEnemyClass(UObject* WorldContextObject, ECharacterClass CharacterClass, int32 Level)
{
	URPGCharacterClassInfo* CharacterClassInfo = GetCharacterClassInfo(WorldContextObject);
	
	if(CharacterClassInfo == nullptr) return 0;
	
	const FCharacterClassDefaultInfo& ClassDefaultInfo = CharacterClassInfo->GetClassDefaultInfo(CharacterClass);
	const float XPReward = ClassDefaultInfo.XPReward.GetValueAtLevel(Level);
	return static_cast<int32>(XPReward);
}

URPGAbilityInfo* URPGAbilitySystemLibrary::GetAbilityInfo(UObject* WorldContextObject)
{
	const ARPGGameModeBase* RPGGameMode = Cast<ARPGGameModeBase>(UGameplayStatics::GetGameMode(WorldContextObject));
	if(RPGGameMode == nullptr) return nullptr;

	return RPGGameMode->AbilityInfo;
}

FGameplayEffectContextHandle URPGAbilitySystemLibrary::ApplyDamageEffect(const FDamageEffectParams& DamageEffectParams)
{
	const FRPG_GameplayTags& GameplayTags = FRPG_GameplayTags::Get();
	const AActor* SourceAvatarActor = DamageEffectParams.SourceAbilitySystemComponent->GetAvatarActor();

	FGameplayEffectContextHandle EffectContextHandle = DamageEffectParams.SourceAbilitySystemComponent->MakeEffectContext();
	EffectContextHandle.AddSourceObject(SourceAvatarActor);
	SetDeathImpulse(EffectContextHandle,DamageEffectParams.DeathImpulse);
	SetKnockBackForce(EffectContextHandle,DamageEffectParams.KnockBackForce);

	SetIsRadialDamage(EffectContextHandle,DamageEffectParams.bIsRadialDamage);
	SetRadialInnerRadius(EffectContextHandle,DamageEffectParams.RadialDamageInnerRadius);
	SetRadialOuterRadius(EffectContextHandle,DamageEffectParams.RadialDamageOuterRadius);
	SetRadialOrigin(EffectContextHandle,DamageEffectParams.RadialDamageOrigin);
	
	const FGameplayEffectSpecHandle SpecHandle = DamageEffectParams.SourceAbilitySystemComponent->MakeOutgoingSpec(DamageEffectParams.DamageGameplayEffectClass,DamageEffectParams.AbilityLevel,EffectContextHandle);

	UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(SpecHandle,DamageEffectParams.DamageType,DamageEffectParams.BaseDamage);
	UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(SpecHandle,GameplayTags.Debuff_Chance,DamageEffectParams.DebuffChance);
	UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(SpecHandle,GameplayTags.Debuff_Damage,DamageEffectParams.DebuffDamage);
	UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(SpecHandle,GameplayTags.Debuff_Duration,DamageEffectParams.DebuffDuration);
	UAbilitySystemBlueprintLibrary::AssignTagSetByCallerMagnitude(SpecHandle,GameplayTags.Debuff_Frequency,DamageEffectParams.DebuffFrequency);

	DamageEffectParams.TargetAbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*SpecHandle.Data);

	return EffectContextHandle;
	
}

TArray<FRotator> URPGAbilitySystemLibrary::EvenlySpacedRotators(const FVector& Forward, const FVector& Axis, float Spread, int32 NumRotators)
{
	TArray<FRotator> Rotators;
	const FVector LeftOfSpread = Forward.RotateAngleAxis(-Spread /2.f,Axis);
	if(NumRotators > 1)
	{
		const float DeltaSpread = Spread / (NumRotators - 1);
        	for(int32 i = 0; i < NumRotators; ++i)
        	{
        		const FVector Direction = LeftOfSpread.RotateAngleAxis(DeltaSpread * i, FVector::UpVector);
				Rotators.Add(Direction.Rotation());
        	}
	}
	else
	{
		Rotators.Add(Forward.Rotation());
	}
	return Rotators;
}

TArray<FVector> URPGAbilitySystemLibrary::EvenlyRotatedVectors(const FVector& Forward, const FVector& Axis, float Spread, int32 NumVectors)
{
	TArray<FVector> Vectors;
	const FVector LeftOfSpread = Forward.RotateAngleAxis(-Spread /2.f,Axis);
	if(NumVectors > 1)
	{
		const float DeltaSpread = Spread / (NumVectors - 1);
		for(int32 i = 0; i < NumVectors; ++i)
		{
			const FVector Direction = LeftOfSpread.RotateAngleAxis(DeltaSpread * i, FVector::UpVector);
			Vectors.Add(Direction);
		}
	}
	else
	{
		Vectors.Add(Forward);
	}
	return Vectors;
}

void URPGAbilitySystemLibrary::SetIsRadialDamageEffectParams(FDamageEffectParams& DamageEffectParams, bool bIsRadial,float InnerRadius, float OuterRadius, FVector Origin)
{
	DamageEffectParams.bIsRadialDamage = bIsRadial;
	DamageEffectParams.RadialDamageInnerRadius = InnerRadius;
	DamageEffectParams.RadialDamageOuterRadius = OuterRadius;
	DamageEffectParams.RadialDamageOrigin = Origin;
}

void URPGAbilitySystemLibrary::SetKnockbackDirection(FDamageEffectParams& DamageEffectParams,
	FVector KnockbackDirection,float Magnitude)
{
	KnockbackDirection.Normalize();
	if(Magnitude == 0.f)
	{
		DamageEffectParams.KnockBackForce = KnockbackDirection * DamageEffectParams.KnockBackForceMagnitude;
	}
	else
	{
		DamageEffectParams.KnockBackForce = KnockbackDirection * Magnitude;
	}
}

void URPGAbilitySystemLibrary::SetDeathImpulseDirection(FDamageEffectParams& DamageEffectParams,
	FVector DeathImpulseDirection,float Magnitude)
{
	DeathImpulseDirection.Normalize();
	if(Magnitude == 0.f)
	{
		DamageEffectParams.DeathImpulse = DeathImpulseDirection * DamageEffectParams.DeathImpulseMagnitude;
	}
	else
	{
		DamageEffectParams.DeathImpulse = DeathImpulseDirection * Magnitude;
	}
}

void URPGAbilitySystemLibrary::SetEffectParamsASC(FDamageEffectParams& DamageEffectParams,
	UAbilitySystemComponent* InASC)
{
	DamageEffectParams.TargetAbilitySystemComponent = InASC;
}
