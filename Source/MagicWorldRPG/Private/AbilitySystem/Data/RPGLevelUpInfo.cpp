// Copyright Alex Shabynin


#include "AbilitySystem/Data/RPGLevelUpInfo.h"

int32 URPGLevelUpInfo::FindLevelForXP(int32 XP) const
{
	int32 Level = 1;
	bool bSearching = true;
	while(bSearching)
	{
		if(LevelUpInformation.Num() -1 <= Level) return Level;
		if(XP >= LevelUpInformation[Level].LevelUpRequirements)
		{
			++Level;
		}
		else
		{
			bSearching = false;
		}
	}
	return Level;
}
