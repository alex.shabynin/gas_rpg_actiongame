// Copyright Alex Shabynin


#include "AbilitySystem/Data/RPGAbilityInfo.h"

#include "MagicWorldRPG/RPG_LogChannels.h"

FRPG_AbilityInfo URPGAbilityInfo::FindAbilityInfoByTag(const FGameplayTag& AbilityTag, bool bLogNotFound) const
{

	for(const FRPG_AbilityInfo& Info : AbilityInformation)
	{
		if(Info.AbilityTag == AbilityTag)
		{
			return Info;
		}
	}
	if(bLogNotFound)
	{
		UE_LOG(LogRPG,Error,TEXT("Can't find info for AbilityTag [%s] on AbilityInfo [%s]."),*AbilityTag.ToString(),*GetNameSafe(this));
	}

	return FRPG_AbilityInfo();
}
