// Copyright Alex Shabynin


#include "AbilitySystem/Data/RPGCharacterClassInfo.h"

FCharacterClassDefaultInfo URPGCharacterClassInfo::GetClassDefaultInfo(ECharacterClass CharacterClass)
{
	return CharacterClassInformation.FindChecked(CharacterClass);
}
