// Copyright Alex Shabynin


#include "AbilitySystem/Abilities/RPGFireBlastSpell.h"

#include "AbilitySystem/RPGAbilitySystemLibrary.h"
#include "Actors/RPGFireBall.h"

FString URPGFireBlastSpell::GetDescription(int32 Level)
{
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);
	
	return FString::Printf(TEXT(
    	"<Small>Level: </><Level>%d</>\n"
            			
        "<Small>ManaCost: </><ManaCost>%.1f</>\n"
            			
        "<Small>Cooldown: </><Cooldown>%.1f</>\n\n"
            
        "<Default>Launches %d </>"
        "<Default>fire balls in all directions, each coming back and </>"
        "<Default>exploding on return, causing </>"
            
        "<Damage>%d</><Default>radial fire damage with"
        " a chance to burn</>"),Level,ManaCost,Cooldown,NumFireBalls,InDamage);
}

FString URPGFireBlastSpell::GetNextLevelDescription(int32 Level)
{
	return GetDescription(Level);
}

TArray<ARPGFireBall*> URPGFireBlastSpell::SpawnFireBalls()
{
	TArray<ARPGFireBall*> FireBalls;
	const FVector Forward = GetAvatarActorFromActorInfo()->GetActorForwardVector();
	const FVector Location = GetAvatarActorFromActorInfo()->GetActorLocation();
	TArray<FRotator> Rotators = URPGAbilitySystemLibrary::EvenlySpacedRotators(Forward,FVector::UpVector,360.f,NumFireBalls);

	for (const FRotator Rotator : Rotators)
	{
		FTransform SpawnTransform;
		SpawnTransform.SetLocation(Location);
		SpawnTransform.SetRotation(Rotator.Quaternion());
		ARPGFireBall* FireBall = GetWorld()->SpawnActorDeferred<ARPGFireBall>(FireballProjectileClass,SpawnTransform,GetOwningActorFromActorInfo(),CurrentActorInfo->PlayerController->GetPawn(),ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
		
		FireBall->DamageEffectParams = MakeDamageEffectParamsFromClassDefaults();
		FireBall->ReturnActor = GetAvatarActorFromActorInfo();
		FireBall->SetOwner(GetAvatarActorFromActorInfo());

		FireBall->ExplosionEffectParams = MakeDamageEffectParamsFromClassDefaults();
		FireBall->SetOwner(GetAvatarActorFromActorInfo());
		FireBalls.Add(FireBall);
		FireBall->FinishSpawning(SpawnTransform);
	}

	return FireBalls;
}
