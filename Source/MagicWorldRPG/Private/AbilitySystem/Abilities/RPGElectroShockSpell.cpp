// Copyright Alex Shabynin


#include "AbilitySystem/Abilities/RPGElectroShockSpell.h"

FString URPGElectroShockSpell::GetDescription(int32 Level)
{
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);
	if(Level == 1)
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Spawns a lightning beam, "
			"shocking enemy on impact and dealing: </>"

			"<Damage>%d</><Default> lightning damage with"
			" a chance to stun an enemy</>"),Level,ManaCost,Cooldown,InDamage);
	}
	else
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Spawns a lightning beam, "
			"propagating to %d additional targets nearby, causing: </>"

			"<Damage>%d</><Default> lightning damage with"
			" a chance to stun an enemy</>"),Level,ManaCost,Cooldown,FMath::Min(Level,MaxNumShockTargets -1),InDamage);
	}
}

FString URPGElectroShockSpell::GetNextLevelDescription(int32 Level)
{
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);

	return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Spawns %d lightning beams, "
			"shocking enemy on impact and dealing: </>"

			"<Damage>%d</><Default> lightning damage with"
			" a chance to stun an enemy</>"),Level,ManaCost,Cooldown,FMath::Min(Level,MaxNumShockTargets),InDamage);
}

