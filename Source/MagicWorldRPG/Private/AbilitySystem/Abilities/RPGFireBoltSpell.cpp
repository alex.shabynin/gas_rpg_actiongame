// Copyright Alex Shabynin


#include "AbilitySystem/Abilities/RPGFireBoltSpell.h"

#include "RPG_GameplayTags.h"
#include "AbilitySystem/RPGAbilitySystemLibrary.h"
#include "Actors/RPGProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"

FString URPGFireBoltSpell::GetDescription(int32 Level)
{
	//const int32 InDamage = GetDamageByDamageType(Level,FRPG_GameplayTags::Get().Damage_Fire);
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);
	if(Level == 1)
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Launches a bolt of fire, "
			"exploding on impact and dealing: </>"

			"<Damage>%d</><Default> fire damage with"
			" a chance to burn</>"),Level,ManaCost,Cooldown,InDamage);
	}
	else
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Launches %d bolts of fire, "
			"exploding on impact and dealing: </>"

			"<Damage>%d</><Default> fire damage with"
			" a chance to burn</>"),Level,ManaCost,Cooldown,FMath::Min(Level,NumProjectiles),InDamage);
	}
	
}

FString URPGFireBoltSpell::GetNextLevelDescription(int32 Level)
{
	//const int32 InDamage = GetDamageByDamageType(Level,FRPG_GameplayTags::Get().Damage_Fire);
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);

	return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Launches %d bolts of fire, "
			"exploding on impact and dealing: </>"

			"<Damage>%d</><Default> fire damage with"
			" a chance to burn</>"),Level,ManaCost,Cooldown,FMath::Min(Level,NumProjectiles),InDamage);
}

void URPGFireBoltSpell::SpawnMultipleProjectiles(const FVector& ProjectileTargetLocation, const FGameplayTag& SocketTag, bool bOverridePitch, float PitchOverride,
	AActor* HomingTarget)
{
	const bool bIsServer = GetAvatarActorFromActorInfo()->HasAuthority();
	if(!bIsServer) return;

	const FVector SocketLocation = IRPG_CombatInterface::Execute_GetCombatSocketLocation(GetAvatarActorFromActorInfo(),SocketTag);

	FRotator Rotation = (ProjectileTargetLocation - SocketLocation).Rotation();

	if(bOverridePitch) Rotation.Pitch = PitchOverride;

	const FVector Forward = Rotation.Vector();
	int32 EffectiveNumProjectiles = FMath::Min(NumProjectiles,GetAbilityLevel());
	
	TArray<FVector> Directions = URPGAbilitySystemLibrary::EvenlyRotatedVectors(Forward, FVector::UpVector,ProjectileSpread,EffectiveNumProjectiles);
	TArray<FRotator> Rotations = URPGAbilitySystemLibrary::EvenlySpacedRotators(Forward,FVector::UpVector,ProjectileSpread,EffectiveNumProjectiles);

	for (const FRotator& Rot : Rotations)
	{
		FTransform SpawnTransform;
		SpawnTransform.SetLocation(SocketLocation);
		SpawnTransform.SetRotation(Rot.Quaternion());

		ARPGProjectile* Projectile = GetWorld()->SpawnActorDeferred<ARPGProjectile>(ProjectileClass,
		SpawnTransform,
		GetOwningActorFromActorInfo(),
		Cast<APawn>(GetOwningActorFromActorInfo()),
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn);

		Projectile->DamageEffectParams = MakeDamageEffectParamsFromClassDefaults();

		if(HomingTarget && HomingTarget->Implements<URPG_CombatInterface>())
		{
			Projectile->ProjectileMovement->HomingTargetComponent = HomingTarget->GetRootComponent();
		}
		else
		{
			Projectile->HomingTargetSceneComponent = NewObject<USceneComponent>(USceneComponent::StaticClass());
			Projectile->HomingTargetSceneComponent->SetWorldLocation(ProjectileTargetLocation);
			Projectile->ProjectileMovement->HomingTargetComponent = Projectile->HomingTargetSceneComponent;
		}
		Projectile->ProjectileMovement->HomingAccelerationMagnitude = FMath::FRandRange(HomingAccelerationMin,HomingAccelerationMax);
		Projectile->ProjectileMovement->bIsHomingProjectile = bLaunchHomingProjectiles;
		
		Projectile->FinishSpawning(SpawnTransform);
	}
	
}
