// Copyright Alex Shabynin


#include "AbilitySystem/Abilities/RPGArcaneShardsSpell.h"

FString URPGArcaneShardsSpell::GetDescription(int32 Level)
{
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);
	if(Level == 1)
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Summon a shard of Arcane energy, "
			"causing radial arcane damage of: </>"

			"<Damage>%d</><Default> at the shard origin.</>"),Level,ManaCost,Cooldown,InDamage);
	}
	else
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Summon %d shards of arcane energy, "
			"causing radial arcane damage of: </>"

			"<Damage>%d</><Default> at the shard origins</>"),Level,ManaCost,Cooldown,FMath::Min(Level,MaxNumArcaneShards),InDamage);
	}
}

FString URPGArcaneShardsSpell::GetNextLevelDescription(int32 Level)
{
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);

	return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Summon %d shards of arcane energy, "
			"causing radial arcane damage of: </>"

			"<Damage>%d</><Default> at the shard origins</>"),Level,ManaCost,Cooldown,FMath::Min(Level,MaxNumArcaneShards),InDamage);
}
