// Copyright Alex Shabynin


#include "AbilitySystem/Abilities/RPGBeamSpell.h"

#include "AbilitySystem/RPGAbilitySystemLibrary.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetSystemLibrary.h"

/*FString URPGBeamSpell::GetDescription(int32 Level)
{
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);
	if(Level == 1)
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Spawns an electrical beam, "
			"shocking enemy on impact and dealing: </>"

			"<Damage>%d</><Default> lightning damage with"
			" a chance to stun an enemy</>"),Level,ManaCost,Cooldown,InDamage);
	}
	else
	{
		return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Spawns %d electric beams, "
			"shocking enemy on impact and dealing: </>"

			"<Damage>%d</><Default> lightning damage with"
			" a chance to stun an enemy</>"),Level,ManaCost,Cooldown,FMath::Min(Level,MaxNumShockTargets),InDamage);
	}
}

FString URPGBeamSpell::GetNextLevelDescription(int32 Level)
{
	const int32 InDamage = Damage.GetValueAtLevel(Level);
	const float ManaCost = FMath::Abs(GetManaCost(Level));
	const float Cooldown = GetCooldown(Level);

	return FString::Printf(TEXT(
			"<Small>Level: </><Level>%d</>\n"
			
			"<Small>ManaCost: </><ManaCost>%.1f</>\n"
			
			"<Small>Cooldown: </><Cooldown>%.1f</>\n\n"

			"<Default>Spawns %d electric beams, "
			"shocking enemy on impact and dealing: </>"

			"<Damage>%d</><Default> lightning damage with"
			" a chance to stun an enemy</>"),Level,ManaCost,Cooldown,FMath::Min(Level,MaxNumShockTargets),InDamage);
}*/

void URPGBeamSpell::StoreMouseDataInfo(const FHitResult& HitResult)
{
	if(HitResult.bBlockingHit)
	{
		MouseHitLocation = HitResult.ImpactPoint;
		MouseHitActor = HitResult.GetActor();
	}
	else
	{
		CancelAbility(CurrentSpecHandle,CurrentActorInfo,CurrentActivationInfo,true);
	}
}

void URPGBeamSpell::StoreOwnerPlayerController()
{
	if(CurrentActorInfo)
	{
		OwnerPlayerController = CurrentActorInfo->PlayerController.Get();
	}
	
}

void URPGBeamSpell::StoreOwnerPlayerCharacter()
{
	OwnerPlayerCharacter = Cast<ACharacter>(CurrentActorInfo->AvatarActor);
}

void URPGBeamSpell::TraceFirstTarget(const FVector& BeamTargetLocation, FHitResult& Hit)
{
	check(OwnerPlayerCharacter);
	if(OwnerPlayerCharacter->Implements<URPG_CombatInterface>())
	{
		if(const USkeletalMeshComponent* Weapon = IRPG_CombatInterface::Execute_GetWeapon(OwnerPlayerCharacter))
		{
			const FVector SocketLocation = Weapon->GetSocketLocation(WeaponSocketName);
			FHitResult TraceOutHit;
			TArray<AActor*> ActorsToIgnore;
            ActorsToIgnore.Add(OwnerPlayerCharacter);
            UKismetSystemLibrary::SphereTraceSingle(OwnerPlayerCharacter,SocketLocation,BeamTargetLocation,25.f,TraceTypeQuery1,false,ActorsToIgnore,EDrawDebugTrace::None,TraceOutHit,true);

			if(TraceOutHit.bBlockingHit)
			{
				MouseHitLocation = TraceOutHit.ImpactPoint;
				MouseHitActor = TraceOutHit.GetActor();
			}
			Hit = TraceOutHit;
		}
		
	}
	if(IRPG_CombatInterface* CombatInterface = Cast<IRPG_CombatInterface>(MouseHitActor))
	{
		if(!CombatInterface->GetOnDeathDelegate().IsAlreadyBound(this,&URPGBeamSpell::PrimaryTargetDead))
		{
			CombatInterface->GetOnDeathDelegate().AddDynamic(this,&URPGBeamSpell::PrimaryTargetDead);
		}
	}
}

void URPGBeamSpell::StoreAdditionalTargets(TArray<AActor*>& OutAdditionalTargets)
{
	TArray<AActor*> IgnoredActors;
	IgnoredActors.Add(GetAvatarActorFromActorInfo());
	IgnoredActors.Add(MouseHitActor);
	TArray<AActor*> OverlappingActors;
	const float ScaledBeamRadius = BeamRadius.GetValueAtLevel(GetAbilityLevel());
	URPGAbilitySystemLibrary::GetLivePlayersWithinRadius(GetAvatarActorFromActorInfo(),OverlappingActors,IgnoredActors,ScaledBeamRadius,MouseHitActor->GetActorLocation());

	int32 NumAdditionalTargets = FMath::Min(GetAbilityLevel() -1,MaxNumShockTargets);
	//int32 NumAdditionalTargets = 5;

	URPGAbilitySystemLibrary::GetClosestTargets(NumAdditionalTargets,OverlappingActors,OutAdditionalTargets,MouseHitActor->GetActorLocation());

	for(AActor* AdditionalTarget : OutAdditionalTargets)
	{
		if(IRPG_CombatInterface* CombatInterface = Cast<IRPG_CombatInterface>(AdditionalTarget))
        	{
        		if(!CombatInterface->GetOnDeathDelegate().IsAlreadyBound(this, &URPGBeamSpell::AdditionalTargetDead))
        		{
        			CombatInterface->GetOnDeathDelegate().AddDynamic(this,&URPGBeamSpell::AdditionalTargetDead);
        		}
        	}
	}
	
}
