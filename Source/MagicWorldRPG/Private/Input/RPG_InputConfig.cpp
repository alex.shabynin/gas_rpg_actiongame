// Copyright Alex Shabynin


#include "Input/RPG_InputConfig.h"

const UInputAction* URPG_InputConfig::FindAbilityInputActionForTag(FGameplayTag& InputTag, bool bLogNotFound) const
{

	for (const FRPGInputAction& Action : AbilityInputActions)
	{
		if(Action.InputAction && Action.InputTag == InputTag)
		{
			return Action.InputAction;
		}
	}
	if (bLogNotFound)
	{
		UE_LOG(LogTemp,Error,TEXT("Can't find AbilityInputAction for InputTag [%s], on Input Config [%s]"),*InputTag.ToString(),*GetNameSafe(this));
	}
	return nullptr;
}
