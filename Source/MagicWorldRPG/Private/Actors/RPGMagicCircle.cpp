// Copyright Alex Shabynin


#include "Actors/RPGMagicCircle.h"

#include "Components/DecalComponent.h"

ARPGMagicCircle::ARPGMagicCircle()
{
	PrimaryActorTick.bCanEverTick = true;

	MagicCircleDecal = CreateDefaultSubobject<UDecalComponent>("MagicCircleDecal");
	MagicCircleDecal->SetupAttachment(GetRootComponent());

}

void ARPGMagicCircle::BeginPlay()
{
	Super::BeginPlay();
	
}

void ARPGMagicCircle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

