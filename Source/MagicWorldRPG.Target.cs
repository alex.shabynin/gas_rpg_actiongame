// Copyright Alex Shabynin

using UnrealBuildTool;
using System.Collections.Generic;

public class MagicWorldRPGTarget : TargetRules
{
	public MagicWorldRPGTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V4;

		ExtraModuleNames.AddRange( new string[] { "MagicWorldRPG" } );
	}
}
